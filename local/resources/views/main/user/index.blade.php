@extends('main.layout.layout')
@section('title-page', 'Beranda')
@section('content')
<section class="content-header">
	<h1>
		Beranda
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>		
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h4>Kelengkapan Data Pendaftaran Bakal Calon</h4>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="row form-group" style="margin-bottom: 0;">
		        <div class="col-xs-12">
		            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
		                <li><a href="{{ asset('user/edit/').'/'.session('kode').'/biodata' }}">
		                    <h4 class="list-group-item-heading">LANGKAH PERTAMA</h4>
		                    <p class="list-group-item-text"><b>FORMULIR BIODATA</b></p>
		                    <i class="fa fa-check" style="color: #4DCC56"></i>
		                </a></li>
		                <li><a href="{{ asset('user/edit/').'/'.session('kode').'/riwayat' }}">
		                    <h4 class="list-group-item-heading">LANGKAH KEDUA</h4>
		                    <p class="list-group-item-text"><b>FORMULIR RIWAYAT HIDUP</b></p>
		                    <i class="fa fa-check" style="color: #4DCC56"></i>
		                </a></li>
		                <li><a href="{{ asset('user/edit/').'/'.session('kode').'/dokumen' }}">
		                    <h4 class="list-group-item-heading">LANGKAH KETIGA</h4>
		                    <p class="list-group-item-text"><b>DOKUMEN PENDUKUNG</b></p>
		                </a></li>
		            </ul>
		        </div>
			</div>
		</div>
	</div>
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h4>Kelengkapan Dokumen</h4>
				</div>
			</div>
		</div>
		<div class="box-body">
			
		</div>
	</div>
</section>
@endsection