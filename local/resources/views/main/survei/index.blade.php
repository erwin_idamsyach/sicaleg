@extends('main.layout.layout')
@section('title-page', 'Hasil Survei - PILKADA')

@section('content')
<section class="content-header">
	<h1>
		Hasil Survei		
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>		
		<li><a href="#"> Hasil Survei</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8">
					<h4>Hasil Survei</h4>
				</div>
				<div class="col-md-4">
					<a href="{{ asset('hasil-survei/input/survei') }}" class="btn btn-warning pull-right">
						<i class="fa fa-pencil"></i> Input Hasil Survei
					</a>
				</div>
			</div>
		</div>
		<div class="box-body"><div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Kategori Kepala Daerah</label>
						<select name="jenkada" id="sel-jenkada" class="form-control">
							<option value="" selected disabled>--Pilih Kategori Kepala Daerah--</option>
							<option value="Gubernur">Gubernur</option>
							<option value="Wakil Gubernur">Gubernur Wakil</option>
							<option value="Bupati">Bupati</option>
							<option value="Wakil Bupati">Wakil Bupati</option>
							<option value="Walikota">Walikota</option>
							<option value="Wakil Walikota">Wakil Walikota</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Provinsi</label>
						<select name="prov" id="arProv" class="form-control" onchange="getKab()">
							<option value="" selected disabled>--Pilih Provinsi--</option>
							@foreach($dataProv as $data)
							<option value="{{ $data->geo_prov_id }}">{{ $data->geo_prov_nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Kota / Kabupaten</label>
						<select name="kota" id="arKota" class="form-control">
							<option value="" selected="" disabled="">--Pilih Kota/Kabupaten--</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<button class="btn btn-warning" style="margin-top: 24px;" onclick="filter()">CARI</button>
				</div>
			</div><hr>
			<table class="table table-hover table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama</th>
						<th class="text-center">Hasil Survei</th>
						<th class="text-center">Provinsi</th>
						<th class="text-center">Kota</th>
						<th class="text-center">Bakal Calon</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody id="area-data-survei">
					<?php $a = 1; ?>
					@foreach($dataBalon as $get)
					<tr>
						<td class="text-center">{{ $a++ }}</td>
						<td>{{ $get->nama }}</td>
						<td class="text-center">
							<?php
							$percentage = ($get->survei / $get->responder)*100;
							echo $percentage." %";
							?>
						</td>
						<td>{{ $get->geo_prov_nama }}</td>
						<td>{{ $get->geo_kab_nama }}</td>
						<td>{{ $get->type }}</td>
						<td></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>
<script>
	function getKab(){
		var isi = $("#arProv").val();
		changeKabOption('#arProv', '#arKota', isi);
	}
	function filter(){
		var jenis = $("#sel-jenkada").val();
		var prov = $("#arProv").val();
		var kota = $("#arKota").val();

		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxDataSurvei') }}",
			data : {
				'jenis' : jenis,
				'prov' : prov,
				'kota' : kota
			},
			success:function(resp){
				$("table").DataTable().destroy();
				$("#area-data-survei").empty();
				$("#area-data-survei").html(resp);
				$("table").DataTable();
			}
		});
	}
</script>
@endsection