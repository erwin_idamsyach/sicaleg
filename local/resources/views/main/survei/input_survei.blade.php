@extends('main.layout.layout')
@section('title-page', 'Input Hasil Survei')

@section('content')
<section class="content-header">
	<h1>
		Input Hasil Survei		
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>		
		<li><a href="#"> Hasil Survei</a></li>
	</ol>
</section>

<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8">
					<h4>Input Hasil Survei</h4>
				</div>
				<div class="col-md-4">
				</div>
			</div>
		</div>
		<div class="box-body">
			<form action="{{ asset('hasil-survei/action/survei/input') }}" method="get">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<table>
				<tr>
					<td>Provinsi</td>
					<td class="pad">:</td>
					<td class="right">
						<select name="arProv" id="arProv" class="form-control" onchange="getKab()">
							<option value="" selected="" disabled="">--Pilih Provinsi--</option>
							@foreach($dataProv as $get)
							<option value="{{ $get->geo_prov_id }}">{{ $get->geo_prov_nama }}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td>Kota/Kabupaten</td>
					<td class="pad">:</td>
					<td class="right">
						<select name="arKota" id="arKota" class="form-control" onchange="writeDown()">
							<option value="" selected="" disabled="">--Pilih Kota/Kabupaten--</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Jumlah Responden</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="number" min="0" step="1" name="jmlResponden" class="form-control inp-responden" required>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<hr>
					</td>
				</tr>
			</table>
			<table class="table-bordered" id="area-calon">
				<tr>
					<td class="text-center pad">No</td>
					<td class="text-center right">Nama Calon</td>
					<td class="text-center pad">Kategori Calon</td>
					<td class="text-center pad">Hasil Survei</td>
				</tr>
				<tbody id="area-bakal-calon">
					
				</tbody>
			</table>
			</form>
		</div>
	</div>
</section>
<input type="hidden" id="det_prov" value="0">
<input type="hidden" id="det_kota" value="0">
<script>
	function getKab(){
		var isi = $("#arProv").val();
		changeKabOption('#arProv', '#arKota', isi);
		$("#det_kota").val(0);
		$("#det_prov").val(isi);
		getListCalon();
	}
	
	function writeDown(){
		var isi = $("#arKota").val();
		$("#det_kota").val(isi);
		getListCalon();
	}

	function getListCalon(){
		var d_prov = $("#det_prov").val();
		var d_kota = $("#det_kota").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxGetListCalon') }}",
			data : {
				'd_prov' : d_prov,
				'd_kota' : d_kota
			},
			success:function(resp){
				$("#area-bakal-calon").html(resp)
			}
		})
	}

	$(".inp-responden").on("keyup", function(){
		var isi = $(".inp-responden").val();
		$(".inp-number").attr('max', isi);
	});
</script>
@endsection