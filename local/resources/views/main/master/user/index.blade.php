@extends('main.layout.layout')
@section('title-page', 'Master User - PUSDATIN PILKADA')

@section('content')
<section class="content-header">
	<h1>
		Master Data
		<small>User</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-sitemap"></i> Master Data</a></li>
		<li><a href="#"> User</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8 col-sm-6">
					<h4>List User</h4>
				</div>
				<div class="col-md-4 col-sm-6">
					<button class="btn btn-warning pull-right" onclick="addUser()"><i class="fa fa-plus"></i> Tambah User</button>
				</div>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Username</th>
						<th class="text-center">Email</th>
						<th class="text-center">Level</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody id="area-data-user">
					<?php $a = 1; ?>
					@foreach($dataUser as $data)
					<tr>
						<td class="text-center"><?php echo $a++; ?></td>
						<td>{{ $data->username }}</td>
						<td>{{ $data->email }}</td>
						<td>{{ $data->level }}</td>
						<td>
							<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View">
								<i class="fa fa-eye"></i>
							</button>&nbsp;&nbsp;
							<button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
								<i class="fa fa-edit"></i>
							</button>&nbsp;&nbsp;
							<button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete">
								<i class="fa fa-trash"></i>
							</button>&nbsp;&nbsp;
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modalUser">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">&times</button>
					<h4 class="modal-title" id="area-title"></h4>
				</div>
				<div class="modal-body">
					<form method="post" id="form-user">
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" id="inp-username" required="">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" id="inp-password" required="">
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" id="inp-email" required="">
						</div>
						<div class="form-group">
							<label>Level</label>
							<select name="level" class="form-control" id="inp-level" required="">
								<option value="" selected="" disabled="">-Pilih Level-</option>
								@foreach($dataLevel as $get)
								<option value="{{ $get->id }}">{{ $get->level }}</option>
								@endforeach
							</select>
						</div>
						<button class="btn btn-warning" id="button-aksi"></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$("#form-user").on("submit", function(){
		var username = $("#inp-username").val();
		var password = $("#inp-password").val();
		var email    = $("#inp-email").val();
		var level    = $("#inp-level").val();

		$.ajax({
			type: "GET",
			url: "../ajaxAddUser",
			data : {
				'username' : username,
				'password' : password,
				'email' : email,
				'level' : level
			},
			success:function(resp){
				load_data();
			}
		});

		return false;
	});
	function load_data(){
		$("#modalUser").modal('hide');
		$("#area-data-user").load("{{ asset('load-data-user') }}");
	}
	function addUser(){
		$("#modalUser").modal('show');
		$("#area-title").text('Tambah User');
		$("#button-aksi").text("TAMBAH");
	}
</script>
@endsection