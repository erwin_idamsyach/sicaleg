<?php $a = 1; ?>
@foreach($dataUser as $data)
<tr>
	<td class="text-center"><?php echo $a++; ?></td>
	<td>{{ $data->username }}</td>
	<td>{{ $data->email }}</td>
	<td>{{ $data->level }}</td>
	<td>
		<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View">
			<i class="fa fa-eye"></i>
		</button>&nbsp;&nbsp;
		<button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
			<i class="fa fa-edit"></i>
		</button>&nbsp;&nbsp;
		<button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete">
			<i class="fa fa-trash"></i>
		</button>&nbsp;&nbsp;
	</td>
</tr>
@endforeach