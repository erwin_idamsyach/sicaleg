<?php $a = 1; ?>
@foreach($dataLevel as $data)
<tr>
	<td class="text-center"><?php echo $a++; ?></td>
	<td>{{ $data->level }}</td>
	<td class="text-right">
		<button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick="editLevel('{{ $data->id }}')">
			<i class="fa fa-edit"></i>
		</button>&nbsp;&nbsp;
		<button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteLevel('{{ $data->id }}')">
			<i class="fa fa-trash"></i>
		</button>&nbsp;&nbsp;
	</td>
</tr>
@endforeach