@extends('main.layout.layout')
@section('title-page', 'Master Level User - PUSDATIN PILKADA')

@section('content')
<section class="content-header">
	<h1>
		Master Data
		<small>Level User</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-sitemap"></i> Master Data</a></li>
		<li><a href="#"> Level User</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8 col-sm-6">
					<h4>List Level User</h4>
				</div>
				<div class="col-md-4 col-sm-6">
					<button class="btn btn-warning pull-right" onclick="addLevel()"><i class="fa fa-plus"></i> Tambah Level User</button>
				</div>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Level User</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody id="area-data-level">
					<?php $a = 1; ?>
					@foreach($dataLevel as $data)
					<tr>
						<td class="text-center"><?php echo $a++; ?></td>
						<td>{{ $data->level }}</td>
						<td class="text-right">
							<button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick="editLevel('{{ $data->id }}')">
								<i class="fa fa-edit"></i>
							</button>&nbsp;&nbsp;
							<button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteLevel('{{ $data->id }}')">
								<i class="fa fa-trash"></i>
							</button>&nbsp;&nbsp;
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modalLevelUser">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="area-modal-title"></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Level User</label>
						<input type="hidden" id="hidden" name="id_level">
						<input type="text" name="level_user" id="level-user" class="form-control">
					</div>
					<button class="btn btn-warning" id="button-aksi"></button>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function loadData(){
		$("#modalLevelUser").modal('hide');
		$("#area-data-level").load("{{ asset('load-data-level') }}", function(){});
	}
	function addLevel(){
		$("#modalLevelUser").modal('show');
		$("#area-modal-title").text("Tambah Level User");
		$("#button-aksi").text("TAMBAH");
		$("#button-aksi").attr("onclick", "aksiLevel('TAMBAH')");
	}
	function editLevel(id){
		/*alert(id);*/
		$.ajax({
			type : "get",
			url  : '../ajaxGetLevel',
			dataType : "json",
			data : 'id='+id,
			success:function(resp){
				$("#modalLevelUser").modal('show');
				$("#area-modal-title").text("Edit Level User");
				$("#button-aksi").text("EDIT");
				$("#button-aksi").attr("onclick", "aksiLevel('EDIT')");
				$("#level-user").val(resp['level']);
				$("#hidden").val(resp['id']);
			}
		})
	}
	function deleteLevel(id){
		if(confirm('Apakah anda yakin akan menghapus level ini?')){
			$.ajax({
				type : 'get',
				url  : '../ajaxDeleteLevel',
				data : 'id='+id,
				success:function(resp){
					loadData();
				}
			})
		}else{

		}
	}
	function aksiLevel(key){
		/*alert(key);*/
		var isi = $("#level-user").val();
		if(key == 'TAMBAH'){	
			$.ajax({
				type : 'get',
				url  : '../ajaxAddLevel',
				data : 'isi='+isi,
				success:function(resp){
					loadData();
				}
			});	
		}else{
			var id = $("#hidden").val();
			$.ajax({
				type : 'get',
				url  : '../ajaxEditLevel',
				data : {
						'isi' : isi,
						'id'  : id
						},
				success:function(resp){
					loadData();
				}
			})
		}
	}
</script>
@endsection