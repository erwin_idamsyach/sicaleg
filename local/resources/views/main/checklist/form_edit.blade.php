@extends('main.layout.layout')
@section('title-page', 'Edit Kelengkapan Dokumen')

@section('content')
<section class="content-header">
	<h1>
		Checklist
		<small>Edit Kelengkapan Dokumen</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Checklist</a></li>
	</ol>
</section>
@foreach($dataUser as $get)
@endforeach
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-12">
					<h4>Form Edit Kelengkapan Dokumen</h4>
				</div>
			</div>
		</div>
		<form action="{{ asset('checklist/action/edit').'/'.$key }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="box-body">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="daftarRiwayatHidup"
							@if($get->file_riwayat != "" || $get->file_riwayat != null) checked='true' @else  @endif
							/>
							Daftar Riwayat Hidup
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filedaftarRiwayatHidup" id="filedaftarRiwayatHidup" class="form-control" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="visiMisi"
							@if($get->file_visi != "" || $get->file_visi != null) checked='true' @else  @endif
							/> Visi - Misi</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filevisiMisi" class="form-control" id="filevisiMisi" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="fotoCopyKtp"
							@if($get->file_ktp != "" || $get->file_ktp != null) checked='true' @else  @endif
							/> Foto Copy KTP</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filefotoCopyKtp" class="form-control" id="filefotoCopyKtp" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="fotoCopyKk"
							@if($get->file_kk != "" || $get->file_kk != null) checked='true' @else  @endif
							/> Foto Copy Kartu Keluarga</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filefotoCopyKk" class="form-control" id="filefotoCopyKk" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="fotoCopyNpwp"
							@if($get->file_npwp != "" || $get->file_npwp != null) checked='true' @else  @endif
							/> Foto Copy NPWP</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filefotoCopyNpwp" class="form-control" id="filefotoCopyNpwp" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="fotoCopyIjazah"
							@if($get->file_ijazah != "" || $get->file_ijazah != null) checked='true' @else  @endif
							/> Foto Copy Ijazah Pendidikan SD - Pendidikan terakhir</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filefotoCopyIjazah" class="form-control" id="filefotoCopyIjazah" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="skcs"
							@if($get->file_skck != "" || $get->file_skck != null) checked='true' @else  @endif
							/> SKCS</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="fileskcs" class="form-control" id="fileskcs" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="ktaPartaiHanura"
							@if($get->file_kta != "" || $get->file_kta != null) checked='true' @else  @endif
							/> KTA Partai Hanura</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filektaPartaiHanura" class="form-control" id="filektaPartaiHanura" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="formPendaftaranCalon"
							@if($get->file_pendaftaran != "" || $get->file_pendaftaran != null) checked='true' @else  @endif
							/> Formulir Pendaftaran Calon</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="fileformPendaftaranCalon" class="form-control" id="fileformPendaftaranCalon" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="komitmen"
							@if($get->file_komitmen != "" || $get->file_komitmen != null) checked='true' @else  @endif
							/> Lembar Komitmen Pencalonan</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filekomitmen" class="form-control" id="filekomitmen" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="bertaqwa"/> Surat Pernyataan Bertaqwa kepada Tuhan Yang Maha Esa</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filebertaqwa" class="form-control" id="filebertaqwa" disabled></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-6 col-sm-6 col-xs-12"><input type="checkbox" id="tinggal"
							@if($get->file_nkri != "" || $get->file_nkri != null) checked='true' @else  @endif
							/> Surat Pernyataan Tempat Tinggal dalam wilayah NKRI</label>
							<div class="col-md-6 col-sm-6 col-xs-12"><input type="file" name="filetinggal" class="form-control" id="filetinggal" disabled></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-md-offset-6 col-sm-offset-6">
							<button class="btn btn-warning">SUBMIT</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		</form>
	</div>
</section>
<script>
$('#daftarRiwayatHidup').click(function() {
if ($(this).is(':checked')) { $('#filedaftarRiwayatHidup').removeAttr('disabled'); } else { $('#filedaftarRiwayatHidup').attr('disabled', 'disabled'); }
});
$('#visiMisi').click(function() {
	if ($(this).is(':checked')) { $('#filevisiMisi').removeAttr('disabled'); } else { $('#filevisiMisi').attr('disabled', 'disabled'); }
});
$('#fotoCopyKtp').click(function() {
	if ($(this).is(':checked')) { $('#filefotoCopyKtp').removeAttr('disabled'); } else { $('#filefotoCopyKtp').attr('disabled', 'disabled'); }
});
$('#fotoCopyKk').click(function() {
	if ($(this).is(':checked')) { $('#filefotoCopyKk').removeAttr('disabled'); } else { $('#filefotoCopyKk').attr('disabled', 'disabled'); }
});
$('#fotoCopyNpwp').click(function() {
	if ($(this).is(':checked')) { $('#filefotoCopyNpwp').removeAttr('disabled'); } else { $('#filefotoCopyNpwp').attr('disabled', 'disabled'); }
});
$('#fotoCopyIjazah').click(function() {
	if ($(this).is(':checked')) { $('#filefotoCopyIjazah').removeAttr('disabled'); } else { $('#filefotoCopyIjazah').attr('disabled', 'disabled'); }
});
$('#skcs').click(function() {
	if ($(this).is(':checked')) { $('#fileskcs').removeAttr('disabled'); } else { $('#fileskcs').attr('disabled', 'disabled'); }
});
$('#ktaPartaiHanura').click(function() {
	if ($(this).is(':checked')) { $('#filektaPartaiHanura').removeAttr('disabled');	} else { $('#filektaPartaiHanura').attr('disabled', 'disabled'); }
});
$('#formPendaftaranCalon').click(function() {
	if ($(this).is(':checked')) { $('#fileformPendaftaranCalon').removeAttr('disabled'); } else { $('#fileformPendaftaranCalon').attr('disabled', 'disabled'); }
});
$('#komitmen').click(function() {
	if ($(this).is(':checked')) { $('#filekomitmen').removeAttr('disabled'); } else { $('#filekomitmen').attr('disabled', 'disabled'); }
});
$('#bertaqwa').click(function() {
	if ($(this).is(':checked')) { $('#filebertaqwa').removeAttr('disabled'); } else { $('#filebertaqwa').attr('disabled', 'disabled'); }
});
$('#tinggal').click(function() {
	if ($(this).is(':checked')) { $('#filetinggal').removeAttr('disabled');	} else { $('#filetinggal').attr('disabled', 'disabled'); }
});
$('#kaderYa').click(function() {
	if ($(this).is(':checked')) { $('#kaderTidak').removeAttr('checked'); $('#responseKader').slideDown(200); } else { $('#kaderYa').attr('checked', 'checked'); $('#responseKader').slideUp(200); }
});
$('#kaderTidak').click(function() {
	if ($(this).is(':checked')) { $('#kaderYa').removeAttr('checked'); $('#responseKader').slideUp(200); } else { $('#kaderTidak').attr('checked', 'checked');  $('#responseKader').slideUp(200);}
});
</script>
@endsection