@extends('main.layout.layout')
@section('title-page', 'Checklist Kelengkapan Dokumen - SICALEG')

@section('content')
<section class="content-header">
	<h1>
		Checklist
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Checklist</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8">
					<h4>List Kelengkapan Dokumen</h4>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
		<div class="box-body">
			@include('main.layout.filter')
			<hr>
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama</th>
						<th class="text-center">Kategori</th>
						<th class="text-center">Dapil</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Daftar Riwayat Hidup">1</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Visi-Misi">2</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Foto Copy KTP">3</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Foto Copy KK">4</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Foto Copy NPWP">5</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Ijazah SD - Pendidikan Terakhir">6</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="SKCK">7</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="KTA HANURA (khusus kader)">8</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Formulir Pendaftaran Calon">9</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Lembar Komitmen Pencalonan">10</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Surat Pernyataan Bertaqwa Kepada Tuhan Yang Maha Esa">11</div>
						</th>
						<th class="text-center">
							<div data-toggle="tooltip" data-placement="top" title="Surat Pernyataan Tinggal di Wilayah NKRI">12</div>
						</th>
						<th class="text-right">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="area-data-balon">
					<?php $a = 1; ?>
					@foreach($dataBalon as $get)
					<tr>
						<td class="text-center"><?php echo $a++; ?></td>
						<td>{{ $get->nama }}</td>
						<td>{{ $get->tingkat_caleg }}</td>
						<td>
							@if($get->type == 1)
							<?php
							$getDapil = DB::table('dapil_dpr_ri')->where('dapil_id', $get->dapil)->get();
							foreach ($getDapil as $d) {
								echo $d->nama_dapil;
							}
							?>
							@elseif($get->type == 2)
							<?php
							$getDapil = DB::table('dapil_dprd_1')->where('dapil_id', $get->dapil)->get();
							foreach ($getDapil as $d) {
								echo $d->nama_dapil;
							}
							?>
							@else
							<?php
							$getDapil = DB::table('dapil_dprd_2')->where('dapil_id', $get->dapil)->get();
							foreach ($getDapil as $d) {
								echo $d->nama_dapil;
							}
							?>
							@endif
						</td>
						<td @if($get->file_riwayat != "" || $get->file_riwayat != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_visi != "" || $get->file_visi != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_ktp != "" || $get->file_ktp != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_kk != "" || $get->file_kk != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_npwp != "" || $get->file_npwp != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_ijazah != "" || $get->file_ijazah != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_skck != "" || $get->file_skck != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_kta != "" || $get->file_kta != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_pendaftaran != "" || $get->file_pendaftaran != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_komitmen != "" || $get->file_komitmen != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_peryataan != "" || $get->file_peryataan != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td @if($get->file_nkri != "" || $get->file_nkri != null)bgcolor='#FF9C00' @else bgcolor='red' @endif></td>
						<td class="text-right">
							<a href="{{ asset('edit/dokumen').'/'.$get->id }}" class="btn btn-warning">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="box box-warning">
		<div class="box-header">
			<h4>Legend</h4>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-xs-1">
					<div style="width: 100%; height: 20px; background-color: #FF9C00"></div> 
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					: Sudah dilengkapi
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1">
					<div style="width: 100%; height: 20px; background-color: red"></div>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					: Belum dilengkapi
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function getKab(){
		var isi = $("#arProv").val();
		changeKabOption('#arProv', '#arKota', isi);
	}
	function filter(){
		var jenis = $("#sel-jenkada").val();
		var prov = $("#arProv").val();
		var kota = $("#arKota").val();

		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxDataChecklist') }}",
			data : {
				'jenis' : jenis,
				'prov' : prov,
				'kota' : kota
			},
			success:function(resp){
				$("table").DataTable().destroy();
				$("#area-data-balon").empty();
				$("#area-data-balon").html(resp);
				$("table").DataTable();
			}
		});
	}
</script>
@endsection