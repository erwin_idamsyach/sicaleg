<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title-page')</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="{{ asset('asset/bootstrap/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('asset/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{asset('asset/css/style-2.css')}}">
</head>
<body>
	@yield('content')
</body>
<script src="{{asset('asset/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
@yield('function')
</html>