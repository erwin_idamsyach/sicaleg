<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    @if(session('role') == " 1")
    <li class="treeview {{ ($menu == 'dashboard')?'active':'' }}">
    	<a href="{{ asset('dashboard') }}">
    		<i class="fa fa-dashboard"></i> Beranda
    	</a>
    </li>
    <li class="treeview {{ ($menu == 'agenda')?'active':'' }}">
    	<a href="{{ asset('agenda') }}">
    		<i class="fa fa-calendar"></i>&nbsp;Agenda
    	</a>
    </li>
    <li  class="treeview {{ ($menu == 'balon_list')?'active':'' }}">
        <a href="{{ asset('bakal-calon/list') }}">
            <i class="fa fa-file"></i> List Bakal Caleg
        </a>
    </li>
    <!-- <li class="treeview {{ ($menu == 'balon_list' || $menu == 'balon-input')?'active':'' }}">
        <a href="#">
            <i class="fa fa-edit"></i> Input Balon
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li  class="treeview {{ ($menu == 'balon_list')?'active':'' }}">
                <a href="{{ asset('bakal-calon/list') }}">
                    <i class="fa fa-circle-o"></i> List Bakal Calon
                </a>
            </li>
            <li class="treeview {{ ($menu == 'balon-input')?'active':'' }}">
                <a href="{{ asset('bakal-calon/input') }}">
                    <i class="fa fa-circle-o"></i> Input Bakal Calon
                </a>
            </li>
        </ul>
    </li> -->
    <li class="treeview {{ ($menu == 'checklist')?'active':'' }}">
        <a href="{{ asset('checklist') }}">
            <i class="fa fa-check"></i> Check List
        </a>
    </li>
    <li class="treeview {{ ($menu == 'hasil-survei')?'active':'' }}">
    	<a href="{{ asset('hasil-survei') }}">
    		<i class="fa fa-trophy"></i> Hasil Survei
    	</a>
    </li>
    <li class="treeview {{ ($menu == 'sk-kokab' || $menu == 'sk-provinsi')?'active':'' }}">
		<a href="#">
			<i class="fa fa-edit"></i> Input SK
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li class="treeview {{ ($menu == 'sk-kokab')?'active':'' }}">
				<a href="{{ asset('sk-kokab') }}">
					<i class="fa fa-circle-o"></i> Bakal Caleg DPR RI
				</a>
			</li>
			<li class="treeview {{ ($menu == 'sk-provinsi')?'active':'' }}">
				<a href="{{ asset('sk-provinsi') }}">
					<i class="fa fa-circle-o"></i> Bakal Caleg DPRD Tingkat I
				</a>
			</li>
            <li class="treeview {{ ($menu == 'sk-provinsi')?'active':'' }}">
                <a href="{{ asset('sk-provinsi') }}">
                    <i class="fa fa-circle-o"></i> Bakal Caleg DPRD Tingkat II
                </a>
            </li>
		</ul>
	</li>
    <!-- <li class="treeview {{ ($menu == 'seleksi-kokab' || $menu == 'seleksi-provinsi')?'active':'' }}">
        <a href="#">
            <i class="fa fa-check"></i> Seleksi Pileg
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="treeview {{ ($menu == 'seleksi-kokab')?'active':'' }}">
                <a href="{{ asset('seleksi/kota') }}">
                    <i class="fa fa-circle-o"></i> Caleg DPR RI
                </a>
            </li>
            <li class="treeview {{ ($menu == 'seleksi-provinsi')?'active':'' }}">
                <a href="{{ asset('seleksi/provinsi') }}">
                    <i class="fa fa-circle-o"></i> Caleg DPRD Tingkat I
                </a>
            </li>
            <li class="treeview {{ ($menu == 'seleksi-provinsi')?'active':'' }}">
                <a href="{{ asset('seleksi/provinsi') }}">
                    <i class="fa fa-circle-o"></i> Caleg DPRD Tingkat II
                </a>
            </li>
        </ul>
    </li> -->
    <li class="treeview {{ ($menu == 'komitmen-kokab' || $menu == 'komitmen-provinsi')?'active':'' }}">
    	<a href="#">
    		<i class="fa fa-edit"></i> Komitmen Caleg
    		<span class="pull-right-container">
    			<i class="fa fa-angle-left pull-right"></i>
    		</span>
    	</a>
    	<ul class="treeview-menu">
    		<li class="treeview {{ ($menu == 'komitmen-kokab')?'active':'' }}">
    			<a href="{{ asset('komitmen-kokab') }}">
    				<i class="fa fa-circle-o"></i> Caleg DPR RI
    			</a>
    		</li>
			<li class="treeview {{ ($menu == 'komitmen-provinsi')?'active':'' }}">
				<a href="{{ asset('komitmen-provinsi') }}">
					<i class="fa fa-circle-o"></i> Caleg DPRD Tingkat I
				</a>
			</li>
            <li class="treeview {{ ($menu == 'komitmen-provinsi')?'active':'' }}">
                <a href="{{ asset('komitmen-provinsi') }}">
                    <i class="fa fa-circle-o"></i> Caleg DPRD Tingkat II
                </a>
            </li>
    	</ul>
    </li>
    <li class="treeview {{ ($menu == 'menu-level' || $menu == 'menu-agenda')?'active':'' }}">
        <a href="#">
            <i class="fa fa-sitemap"></i> Master Data
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="treeview {{ ($menu == 'menu-level')?'active':'' }}">
                <a href="{{ asset('master/level') }}">
                    <i class="fa fa-circle-o"></i> Level User
                </a>
            </li>
            <li class="treeview {{ ($menu == 'menu-agenda')?'active':'' }}">
                <a href="{{ asset('master/agenda') }}">
                    <i class="fa fa-circle-o"></i> Agenda
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ ($menu == 'menu-user')?'active':'' }}">
        <a href="{{ asset('master/user') }}">
            <i class="fa fa-user"></i> User Management
        </a>
    </li>
    <!-- <li class="treeview {{ ($menu == 'kelengkapan' || $menu == 'caleg-terpilih' || $menu == 'caleg-hanura' || $menu == 'perbandingan-caleg')?'active':'' }}">
        <a href="#">
            <i class="fa fa-book"></i> Report
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="treeview {{ ($menu == 'kelengkapan')?'active':'' }}">
                <a href="{{ asset('report/kelengkapan') }}">
                    <i class="fa fa-circle-o"></i> Kelengkapan
                </a>
            </li>
            <li class="treeview {{ ($menu == 'caleg-terpilih')?'active':'' }}">
                <a href="{{ asset('report/caleg-terpilih') }}">
                    <i class="fa fa-circle-o"></i> Caleg Terpilih
                </a>
            </li>
            <li class="treeview {{ ($menu == 'caleg-hanura')?'active':'' }}">
                <a href="{{ asset('report/caleg-hanura') }}">
                    <i class="fa fa-circle-o"></i> Caleg HANURA
                </a>
            </li>
                <li class="treeview {{ ($menu == 'perbandingan-caleg')?'active':'' }}">
                    <a href="{{ asset('report/perbandingan-caleg') }}">
                        <i class="fa fa-circle-o"></i> Perbandingan Caleg Partai
                    </a>
                </li>
        </ul>
    </li> -->
    @elseif(session('role') == 4)
    <li class="{{ ($menu == 'dashboard')?'active':'' }}">
        <a href="{{ asset('user/dashboard') }}">
            <i class="fa fa-dashboard"></i> Beranda
        </a>
    </li>
    <li class="treeview {{ ($menu == 'data')?'active':'' }}">
        <a href="#">
            <i class="fa fa-edit"></i> Perbarui Data
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="{{ asset('user/edit/').'/'.session('kode').'/biodata' }}">
                    <i class="fa fa-circle-o"></i> Biodata
                </a>
            </li>
            <li>
                <a href="{{ asset('user/edit/').'/'.session('kode').'/riwayat' }}">
                    <i class="fa fa-circle-o"></i> Riwayat Hidup
                </a>
            </li>
            <li>
                <a href="{{ asset('user/edit/').'/'.session('kode').'/dokumen' }}">
                    <i class="fa fa-circle-o"></i> Biodata
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ ($menu == 'pengaturan')?'active':'' }}">
        <a href="{{ asset('user/setting') }}">
            <i class="fa fa-gear"></i> Pengaturan
        </a>
    </li>
    @else
    @endif
    <li>
    	<a href="{{ asset('logout') }}">
    		<i class="fa fa-sign-out"></i> Logout
    	</a>
    </li>
</ul>