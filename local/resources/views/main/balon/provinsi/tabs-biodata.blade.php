<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Jenis Pemilihan</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="jenkada" name="jenis_kada" required>
						<option value="">--- Pilih Jenis Kepala Daerah ---</option>
						<option value="Gubernur">Gubernur</option>
						<option value="Wakil Gubernur">Wakil Gubernur</option>
						<option value="Bupati">Bupati</option>
						<option value="Wakil Bupati">Wakil Bupati</option>
						<option value="Walikota">Walikota</option>
						<option value="Wakil Walikota">Wakil Walikota</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Provinsi Penyelenggara</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="provinsi" name="dapil_provinsi" required>
						<option value="">--- Pilih Provinsi Penyelenggara ---</option>
						@foreach($dataProvinsi as $get)
						<option value="{{ $get->geo_prov_id }}">{{ $get->geo_prov_nama }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Identitas</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="identitas" name="identitas" required onchange="cekIdentitas()">
						<option value="">--- Pilih Identitas ---</option>
						<option value="KTP">KTP</option>
						<option value="SIM">SIM</option>
						<option value="Paspor">PASPOR</option>
						<option value="NPWP">NPWP</option>
						<option value="Kartu Pelajar">Kartu Pelajar</option>
						<option value="KK">KK</option>
					</select>
				</div>	
				<div class="col-md-3 col-sm-6 col-xs-12 multi-row">
					<input type="text" class="form-control" id="noIdentitas" name="noIdentitas" placeholder="Nomer Identitas" disabled onkeyup="cekIdentitas()" />
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 multi-row" id="responseCheck"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputEmail3" class="col-md-2 col-sm-2 col-xs-12">Nama</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="namaDepan" class="form-control" id="namaDepan" placeholder="Nama Depan" required>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="namaTengah" class="form-control multi-row" id="namaTengah" placeholder="Nama Tengah">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="namaBelakang" class="form-control multi-row" id="namaBelakang" placeholder="Nama Belakang">
				</div>
			</div>
		</div> 
		<div class="form-group">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Tempat/Tanggal Lahir</label></div>
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
					<select class="form-control" id="tlProv" name="tlProv" required onchange="getKokabTTL()">
						<option value="0">--- Pilih Provinsi ---</option>
						<?php foreach($dataProvinsi as $tmpprop){?>
							<option value="{{ $tmpprop->geo_prov_id }}">{{ $tmpprop->geo_prov_nama }}</option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
					<select class="form-control" id="tlKab" name="tempatLahir" required>
						<option value="0" >--- Pilih Kota/Kabupaten ---</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
					<input type="date" name="tanggalLahir" class="form-control" id="datepicker" placeholder="dd-mm-yyyy" required>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-4 col-xs-12">Agama</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select class="form-control" id="agama" name="agama" required>
						<option value="">--- Pilih ---</option>
							<option value="Islam">Islam</option>
							<option value="Kristen">Kristen</option>
							<option value="Katolik">Katolik</option>
							<option value="Hindu">Hindu</option>
							<option value="Budha">Budha</option>
					</select>
				</div>
				<label for="inputPassword3" class="col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
						<input type="radio" name="jenisKelamin" value="Laki-Laki">Laki-Laki</input>
						<input type="radio" name="jenisKelamin" value="Perempuan">perempuan</input>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Alamat</label>
				<div class="col-md-8 col-sm-8 col-xs-9">
					<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Pilih Lokasi Pada Maps" required>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1 multi-row">
					<div class="btn btn-default btn-custom" id="google_maps_balon" data-toggle="modal" data-target="#modal_maps_balon"><span class="glyphicon glyphicon-search" style="padding:3px;" aria-hidden="true"></span></div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row col-md-offset-2 col-sm-offset-2 col-xs-offset-12">
					<select class="form-control" id="abProv" name="abProv" required>
						<option value="">--- Pilih Provinsi ---</option>
						<?php foreach($dataProvinsi as $tmpprop){?>
							<option value="{{ $tmpprop->geo_prov_id }}">{{ $tmpprop->geo_prov_nama }}</option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
					<select class="form-control" id="abKab" name="abKab" required>
						<option value="" >--- Pilih Kota/Kabupaten ---</option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
					<select class="form-control" id="abKec" name="abKec" required>
						<option value="">--- Pilih Kecamatan ---</option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
					<select class="form-control" id="abKel" name="abKel" required>
						<option value="">--- Pilih Kelurahan ---</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0;">
					<label for="inputEmail3" class="col-md-2 col-sm-6 col-xs-12">Status Pernikahan</label>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<select name="statusPernikahan" id="statusPernikahan" class="form-control" required>
							<option value="">--- Pilih ---</option>
								<option value="belum">Belum Menikah</option>
								<option value="menikah">Menikah</option>
								<option value="Janda/Duda">Janda/Duda</option>
						</select>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="namaPasangan" id="namaPasangan" placeholder="Nama Pasangan" disabled />
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="jumlahAnak" id="jumlahAnak" placeholder="Jumlah Anak" disabled />
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Kontak</label></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="telp" id="telp" class="form-control" placeholder="No. Telp"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="hp" id="hp" class="form-control" placeholder="Handphone"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="email" name="emailBalon" id="emailBalon" class="form-control" placeholder="Email"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-2 col-sm-2"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="twitter" id="twitter" class="form-control" placeholder="Twitter"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="facebook" id="facebook" class="form-control" placeholder="Facebook"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="file" name="foto" id="foto" class="form-control"></div>
			</div>
		</div>
	</div>
</div>