@extends('main.layout.layout')
@section('title-page', 'List Balon Provinsi - PUSDATIN PILKADA')

@section('content')
<section class="content-header">
	<h1>
		Input Balon
		<small>Balon Tingkat Provinsi</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Input Balon</a></li>
		<li><a href="#"> Balon Tingkat Provinsi</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8 col-sm-6">
					<h4>List Bakal Calon Tingkat Provinsi</h4>
				</div>
				<div class="col-md-4 col-sm-6">
					<a href="{{ asset('bakal-calon/provinsi/add') }}" class="btn btn-warning pull-right">
						<i class="fa fa-plus"></i> Tambah Balon
					</a>
				</div>
			</div>
		</div>
		<div class="box-body"><div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Kategori Kepala Daerah</label>
						<select name="jenkada" id="sel-jenkada" class="form-control">
							<option value="" selected>--Pilih Kategori Kepala Daerah--</option>
							<option value="Gubernur">Gubernur</option>
							<option value="Wakil Gubernur">Wakil Gubernur</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Provinsi</label>
						<select name="prov" id="arProv" class="form-control">
							<option value="" selected disabled>--Pilih Provinsi--</option>
							@foreach($dataProv as $data)
							<option value="{{ $data->geo_prov_id }}">{{ $data->geo_prov_nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<button class="btn btn-warning" onclick="filtrate()" id="btnSe" style="margin-top: 24px;">CARI</button>
				</div>
			</div><hr>
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama</th>
						<th class="text-center">Tempat, Tgl. Lahir</th>
						<th class="text-center">Provinsi</th>
						<th class="text-center">Bakal Calon</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody id="area-data">
					<?php $a = 1; ?>
					@foreach($dataBalonProvinsi as $get)
					<tr>
						<td class="text-center"><?php echo $a++; ?></td>
						<td>{{ $get->nama }}</td>
						<td>{{ $get->tempat_lahir.", ".$get->tanggal_lahir }}</td>
						<td>{{ $get->geo_prov_nama }}</td>						
						<td>{{ $get->type }}</td>
						<td class="text-right">
							<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View" onclick="view_calon_provinsi('{{ $get->id }}')">
								<i class="fa fa-eye"></i>
							</button>&nbsp;&nbsp;
							<a href="{{ asset('bakal-calon/provinsi/edit/').'/'.$get->id }}" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
								<i class="fa fa-edit"></i>
							</a>&nbsp;&nbsp;
						</td>
					</tr>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modal-view-balon">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-toggle="modal">&times;</button>
					<h4 class="modal-title">Bakal Calon Provinsi</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<table class="table">
								<tr>
									<td>Kategori Calon</td>
									<td>:</td>
									<td class="area-kat-calon"></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<table class="table">
								<tr>
									<td>Provinsi</td>
									<td>:</td>
									<td class="area-daerah-prov"></td>
								</tr>
							</table>
						</div>
						<div class="col-md-12"><hr></div>
						<div class="col-md-12">
							<table class="table">
								<tr>
									<td>
										<table class="table">
											<tr>
												<td>Jenis Identitas</td>
												<td>:</td>
												<td id="area-jen-id"></td>
											</tr>
											<tr>
												<td>Nama</td>
												<td>:</td>
												<td id="area-nama"></td>
											</tr>
											<tr>
												<td>Tempat Lahir</td>
												<td>:</td>
												<td id="area-tempat"></td>
											</tr>
											<tr>
												<td>Alamat</td>
												<td>:</td>
												<td id="area-alamat"></td>
											</tr>
											<tr>
												<td>Kota/Kabupaten</td>
												<td>:</td>
												<td id="area-kokab"></td>
											</tr>
										</table>
									</td>
									<td>
										<table class="table">
											<tr>
												<td>Nomor Identitas</td>
												<td>:</td>
												<td id="area-no-id"></td>
											</tr>
											<tr>
												<td>Jenis Kelamin</td>
												<td>:</td>
												<td id="area-jenkel"></td>
											</tr>
											<tr>
												<td>Tanggal Lahir</td>
												<td>:</td>
												<td id="area-tanggal"></td>
											</tr>
											<tr>
												<td>Kecamatan</td>
												<td>:</td>
												<td id="area-kecamatan"></td>
											</tr>
											<tr>
												<td>Provinsi</td>
												<td>:</td>
												<td id="area-provinsi"></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function filtrate(){
		var jenis = $("#sel-jenkada").val();
		var prov = $("#arProv").val();

		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxDataBalonProv') }}",
			data : {
				'jenis' : jenis,
				'prov' : prov,
			},
			success:function(resp){
				$("#area-data").empty();			
				$("#area-data").html(resp);
			}
		});
	}

	function view_calon_provinsi(key){
		$.ajax({
			type : "GET",
			url  : "{{ asset('bakal-calon/provinsi/view') }}",
			data : "key="+key,
			dataType : "json",
			success:function(resp){
				$("#modal-view-balon").modal('show');
				$(".area-kat-calon").text(resp['kategori']);
				$(".area-daerah-prov").text(resp['provinsi']);
				$("#area-jen-id").text(resp['jenis_id']);
				$("#area-no-id").text(resp['no_id']);
				$("#area-nama").text(resp['nama']);
				$("#area-tempat").text(resp['tempat_lahir']);
				$("#area-tanggal").text(resp['tgl_lahir']);
				$("#area-alamat").text(resp['alamat']);
				$("#area-kokab").text(resp['kabupaten']);
				$("#area-kecamatan").text(resp['kecamatan']);
				$("#area-provinsi").text(resp['provinsis']);
			}
		});
	}
</script>
@endsection