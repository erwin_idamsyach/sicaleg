@extends('main.layout.layout')
@section('title-page', 'List Balon - PUSDATIN PILKADA')

@section('content')
<section class="content-header">
	<h1>
		List Bakal Calon
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Input Balon</a></li>
		<li><a href="#"> List Bakal Calon</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8 col-sm-6">
					<h4>List Bakal Calon </h4>
				</div>
				<div class="col-md-4 col-sm-6">
					<a href="{{ asset('bakal-calon/kokab/add') }}" class="btn btn-warning pull-right">
						<i class="fa fa-plus"></i> Tambah Balon
					</a>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Kategori Kepala Daerah</label>
						<select name="jenkada" id="sel-jenkada" class="form-control">
							<option value="" selected disabled>--Pilih Kategori Kepala Daerah--</option>
							<option value="Gubernur">Gubernur</option>
							<option value="Wakil Gubernur">Gubernur Wakil</option>
							<option value="Bupati">Bupati</option>
							<option value="Wakil Bupati">Wakil Bupati</option>
							<option value="Walikota">Walikota</option>
							<option value="Wakil Walikota">Wakil Walikota</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Provinsi</label>
						<select name="prov" id="arProv" class="form-control" onchange="getKab()">
							<option value="" selected disabled>--Pilih Provinsi--</option>
							@foreach($dataProv as $data)
							<option value="{{ $data->geo_prov_id }}">{{ $data->geo_prov_nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label>Kota / Kabupaten</label>
						<select name="kota" id="arKota" class="form-control">
							<option value="" selected="" disabled="">--Pilih Kota/Kabupaten--</option>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<button class="btn btn-warning" style="margin-top: 24px;" onclick="filter()">CARI</button>
				</div>
			</div><hr>
			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Nama</th>
						<th class="text-center">Tempat, Tgl. Lahir</th>
						<th class="text-center">Provinsi</th>
						<th class="text-center">Kota/Kabupaten</th>
						<th class="text-center">Bakal Calon</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody id="area-data-balon">
					<?php $a = 1; ?>
					@foreach($dataBalonKokab as $get)
					<tr>
						<td class="text-center"><?php echo $a++; ?></td>
						<td>{{ $get->nama }}</td>
						<td>{{ $get->tempat_lahir.", ".$get->tanggal_lahir }}</td>
						<td>{{ $get->geo_prov_nama }}</td>
						<td>{{ $get->geo_kab_nama }}</td>
						<td>{{ $get->type }}</td>
						<td class="text-right">
							<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View" onclick="view_calon_kokab('{{ $get->id }}')">
								<i class="fa fa-eye"></i>
							</button>&nbsp;&nbsp;
							<a href="{{ asset('bakal-calon/kokab/edit').'/'.$get->id }}" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
								<i class="fa fa-edit"></i>
							</a>&nbsp;&nbsp;
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modal-view-balon">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-toggle="modal">&times;</button>
					<h4 class="modal-title">Bakal Calon Kota/Kabupaten</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<table class="table">
								<tr>
									<td>Kategori Calon</td>
									<td>:</td>
									<td class="area-kat-calon"></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<table class="table">
								<tr>
									<td>Provinsi</td>
									<td>:</td>
									<td class="area-daerah-prov"></td>
								</tr>
							</table>
						</div>
						<div class="col-md-12"><hr></div>
						<div class="col-md-12">
							<table class="table">
								<tr>
									<td>
										<table class="table">
											<tr>
												<td>Jenis Identitas</td>
												<td>:</td>
												<td id="area-jen-id"></td>
											</tr>
											<tr>
												<td>Nama</td>
												<td>:</td>
												<td id="area-nama"></td>
											</tr>
											<tr>
												<td>Tempat Lahir</td>
												<td>:</td>
												<td id="area-tempat"></td>
											</tr>
											<tr>
												<td>Alamat</td>
												<td>:</td>
												<td id="area-alamat"></td>
											</tr>
											<tr>
												<td>Kota/Kabupaten</td>
												<td>:</td>
												<td id="area-kokab"></td>
											</tr>
										</table>
									</td>
									<td>
										<table class="table">
											<tr>
												<td>Nomor Identitas</td>
												<td>:</td>
												<td id="area-no-id"></td>
											</tr>
											<tr>
												<td>Jenis Kelamin</td>
												<td>:</td>
												<td id="area-jenkel"></td>
											</tr>
											<tr>
												<td>Tanggal Lahir</td>
												<td>:</td>
												<td id="area-tanggal"></td>
											</tr>
											<tr>
												<td>Kecamatan</td>
												<td>:</td>
												<td id="area-kecamatan"></td>
											</tr>
											<tr>
												<td>Provinsi</td>
												<td>:</td>
												<td id="area-provinsi"></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function getKab(){
		var isi = $("#arProv").val();
		changeKabOption('#arProv', '#arKota', isi);
	}
	function filter(){
		var jenis = $("#sel-jenkada").val();
		var prov = $("#arProv").val();
		var kota = $("#arKota").val();

		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxDataBalon') }}",
			data : {
				'jenis' : jenis,
				'prov' : prov,
				'kota' : kota
			},
			success:function(resp){
				$("#area-data-balon").empty();
				$("#area-data-balon").html(resp);
			}
		});
	}
	function view_calon_kokab(key){
		$.ajax({
			type : "GET",
			url  : "{{ asset('bakal-calon/provinsi/view') }}",
			data : "key="+key,
			dataType : "json",
			success:function(resp){
				$("#modal-view-balon").modal('show');
				$(".area-kat-calon").text(resp['kategori']);
				$(".area-daerah-prov").text(resp['provinsi']);
				$("#area-jen-id").text(resp['jenis_id']);
				$("#area-no-id").text(resp['no_id']);
				$("#area-nama").text(resp['nama']);
				$("#area-tempat").text(resp['tempat_lahir']);
				$("#area-tanggal").text(resp['tgl_lahir']);
				$("#area-alamat").text(resp['alamat']);
				$("#area-kokab").text(resp['kabupaten']);
				$("#area-kecamatan").text(resp['kecamatan']);
				$("#area-provinsi").text(resp['provinsis']);
			}
		});
	}
</script>
@endsection