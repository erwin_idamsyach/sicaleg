@extends('main.layout.layout')
@section('title-page', 'Add Balon Kota/Kabupaten - PUSDATIN PILKADA')

@section('content')
<section class="content-header">
	<h1>
		Tambah Balon Tingkat Kota/Kabupaten
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Input Balon</a></li>
		<li><a href="#"> Balon Tingkat Kota/Kabupaten</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header"></div>
		<div class="box-body">
			<ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#data-diri" aria-controls="home" role="tab" data-toggle="tab">Data Diri</a></li>
			    <li role="presentation"><a href="#riwayat" aria-controls="profile" role="tab" data-toggle="tab">Riwayat</a></li>
			    <li role="presentation"><a href="#dokumen" aria-controls="messages" role="tab" data-toggle="tab">Dokumen Pendukung</a></li>
			</ul>

			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane fade in active" id="data-diri"><br>
			    	<div class="row">
			    		<div class="col-md-6">
			    			<div class="form-group">
			    				<label>Kategori Calon :</label>
			    				<select class="form-control" name="kategori" required="">
			    					<option value="Walikota">Walikota</option>
			    					<option value="Wakil Walikota">Wakil Walikota</option>
			    					<option value="Bupati">Bupati</option>
			    					<option value="Wakil Bupati">Wakil Bupati</option>
			    				</select>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			    <div role="tabpanel" class="tab-pane fade" id="riwayat">...</div>
			    <div role="tabpanel" class="tab-pane fade" id="dokumen">...</div>
			  </div>
		</div>
	</div>
</section>
@endsection