@extends('main.layout.layout')
@section('title-page', 'Seleksi Caleg')

@section('content')
<section class="content-header">
	<h1>
		Seleksi Pilkada
		<small>Kota/Kabupaten</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-check"></i> Seleksi Pilkada</a></li>
		<li><a href="#">Calon Tingkat Kota/Kabupaten</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8 col-sm-9 col-xs-10">
					<h4>List Calon Tingkat Kota/Kabupaten</h4>
				</div>
				<div class="col-md-4 col-sm-3 col-xs-2">
					<button class="btn btn-warning" type="button">
						<i class="fa fa-plus"></i> 
					</button>
				</div>
			</div>
		</div>
		<div class="box-body">
			
		</div>
	</div>
</section>
@endsection