<div class="row">
@foreach($dataEdit as $data)
@endforeach
<?php 
$nama = explode(" ", $data->nama);
?>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Jenis Pemilihan</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="jenkada" name="jenis_kada" required>
						<option value="">--- Pilih Jenis Kepala Daerah ---</option>
						<option value="Walikota" <?php echo ($data->type == "Walikota")?"selected=''":"" ?>>Walikota</option>
						<option value="Wakil Walikota" <?php echo ($data->type == "Wakil Walikota")?"selected=''":"" ?>>Wakil Walikota</option>						
						<option value="Bupati" <?php echo ($data->type == "Bupati")?"selected=''":"" ?>>Bupati</option>
						<option value="Wakil Bupati" <?php echo ($data->type == "Wakil Bupati")?"selected=''":"" ?>>Wakil Bupati</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Provinsi Penyelenggara</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="provinsi" name="dapil_provinsi" required>
						<option value="">--- Pilih Provinsi Penyelenggara ---</option>
						@foreach($dataProvinsi as $get)
						<option value="{{ $get->geo_prov_id }}" <?php echo ($data->tingkat_provinsi == $get->geo_prov_id)?"selected=''":"" ?>>{{ $get->geo_prov_nama }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Identitas</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="identitas" name="identitas" required onchange="cekIdentitas()">
						<option value="">--- Pilih Identitas ---</option>
						<option value="KTP" <?php echo ($data->jenis_identitas == "KTP")?"selected=''":"" ?>>KTP</option>
						<option value="SIM" <?php echo ($data->jenis_identitas == "SIM")?"selected=''":"" ?>>SIM</option>
						<option value="Paspor" <?php echo ($data->jenis_identitas == "Paspor")?"selected=''":"" ?>>PASPOR</option>
						<option value="NPWP" <?php echo ($data->jenis_identitas == "NPWP")?"selected=''":"" ?>>NPWP</option>
						<option value="Kartu Pelajar" <?php echo ($data->jenis_identitas == "Kartu Pelajar")?"selected=''":"" ?>>Kartu Pelajar</option>
						<option value="KK" <?php echo ($data->jenis_identitas == "KK")?"selected=''":"" ?>>KK</option>
					</select>
				</div>	
				<div class="col-md-3 col-sm-6 col-xs-12 multi-row">
					<input type="text" class="form-control" id="noIdentitas" name="noIdentitas" placeholder="Nomer Identitas" value="{{ $data->nomer_identitas }}" disabled onkeyup="cekIdentitas()" />
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 multi-row" id="responseCheck"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputEmail3" class="col-md-2 col-sm-2 col-xs-12">Nama</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="namaDepan" class="form-control" id="namaDepan" placeholder="Nama Depan" value="{{ $nama[0] }}" required>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="namaTengah" class="form-control multi-row" id="namaTengah" placeholder="Nama Tengah" value="{{ $nama[1] }}">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="namaBelakang" class="form-control multi-row" id="namaBelakang" placeholder="Nama Belakang" value="{{ $nama[2] }}">
				</div>
			</div>
		</div> 
		<div class="form-group">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Tempat/Tanggal Lahir</label></div>
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
					<select class="form-control" id="tlProv" name="tlProv" required onchange="getKokabTTL()">
						<option value="0">--- Pilih Provinsi ---</option>
						<?php foreach($dataProvinsi as $tmpprop){?>
							<option value="{{ $tmpprop->geo_prov_id }}">{{ $tmpprop->geo_prov_nama }}</option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
					<select class="form-control" id="tlKab" name="tempatLahir" required>
						<option value="0" >--- Pilih Kota/Kabupaten ---</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
					<input type="date" name="tanggalLahir" class="form-control" id="datepicker" placeholder="dd-mm-yyyy" value="{{ $data->tanggal_lahir }}" required>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-4 col-xs-12">Agama</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select class="form-control" id="agama" name="agama" required>
						<option value="">--- Pilih ---</option>
							<option value="Islam" <?php echo ($data->agama == "Islam")?"selected":"" ?>>Islam</option>
							<option value="Kristen" <?php echo ($data->agama == "Kristen")?"selected":"" ?>>Kristen</option>
							<option value="Katolik" <?php echo ($data->agama == "Katolik")?"selected":"" ?>>Katolik</option>
							<option value="Hindu" <?php echo ($data->agama == "Hindu")?"selected":"" ?>>Hindu</option>
							<option value="Budha" <?php echo ($data->agama == "Budha")?"selected":"" ?>>Budha</option>
					</select>
				</div>
				<label for="inputPassword3" class="col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
						<input type="radio" name="jenisKelamin" value="Laki-Laki" <?php echo ($data->jenis_kelamin == "Laki-Laki" || $data->jenis_kelamin == "lakilaki")?"checked":"" ?>>Laki-Laki</input>
						<input type="radio" name="jenisKelamin" value="Perempuan" <?php echo ($data->jenis_kelamin == "perempuan" || $data->jenis_kelamin == "Perempuan")?"checked":"" ?>>perempuan</input>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Alamat</label>
				<div class="col-md-8 col-sm-8 col-xs-9">
					<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Pilih Lokasi Pada Maps" value="{{ $data->alamat }}" required>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-1 multi-row">
					<div class="btn btn-default btn-custom" id="google_maps_balon" data-toggle="modal" data-target="#modal_maps_balon"><span class="glyphicon glyphicon-search" style="padding:3px;" aria-hidden="true"></span></div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12 multi-row col-md-offset-2 col-sm-offset-2 col-xs-offset-12">
					<select class="form-control" id="abProv" name="abProv" required>
						<option value="">--- Pilih Provinsi ---</option>
						<?php foreach($dataProvinsi as $tmpprop){?>
							<option value="{{ $tmpprop->geo_prov_id }}" <?php echo ($data->provinsi == $tmpprop->geo_prov_id)?"selected":"" ?>>{{ $tmpprop->geo_prov_nama }}</option>
						<?php }?>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
					<select class="form-control" id="abKab" name="abKab" required>
						<option value="" >--- Pilih Kota/Kabupaten ---</option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
					<select class="form-control" id="abKec" name="abKec" required>
						<option value="">--- Pilih Kecamatan ---</option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
					<select class="form-control" id="abKel" name="abKel" required>
						<option value="">--- Pilih Kelurahan ---</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0;">
					<label for="inputEmail3" class="col-md-2 col-sm-6 col-xs-12">Status Pernikahan</label>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<select name="statusPernikahan" id="statusPernikahan" class="form-control" required>
							<option value="">--- Pilih ---</option>
								<option value="belum" <?php echo ($data->status_kawin == "belum" || $data->status_kawin == "Belum")?"selected":"" ?>>Belum Menikah</option>
								<option value="menikah" <?php echo ($data->status_kawin == "menikah" || $data->status_kawin == "Menikah")?"selected":"" ?>>Menikah</option>
								<option value="Janda/Duda">Janda/Duda</option>
						</select>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="namaPasangan" id="namaPasangan" placeholder="Nama Pasangan" value="{{ $data->pasangan }}" disabled />
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<input type="text" class="form-control" name="jumlahAnak" id="jumlahAnak" placeholder="Jumlah Anak" value="{{ $data->anak }}" disabled />
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Kontak</label></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="telp" id="telp" class="form-control" value="{{ $data->telephone }}" placeholder="No. Telp"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="hp" id="hp" class="form-control" value="{{ $data->handphone }}" placeholder="Handphone"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="email" name="emailBalon" id="emailBalon" class="form-control" value="{{ $data->email }}" placeholder="Email"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-2 col-sm-2"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="twitter" id="twitter" class="form-control" value="{{ $data->twitter }}" placeholder="Twitter"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="facebook" id="facebook" class="form-control" value="{{ $data->facebook }}" placeholder="Facebook"></div>
				<div class="col-md-3 col-sm-3 col-xs-12"><input type="file" name="foto" id="foto" class="form-control"></div>
			</div>
		</div>
	</div>
</div>
<script>
	$(window).on("load", function(){

	});
</script>