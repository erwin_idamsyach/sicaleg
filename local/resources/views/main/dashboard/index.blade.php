@extends('main.layout.layout')
@section('title-page', 'SICALEG')

@section('content')
<section class="content">	
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header">
				</div>
				<div class="box-body" id="map" style="height: 417px;"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box" style="cursor: pointer;" onclick="showMark('ALL')">
				<div class="info-box-icon bg-yellow">
					<i class="fa fa-map" style="margin-top: 20px"></i>
				</div>
				<div class="info-box-content">
					<h4>Total Daerah</h4>
					<h3 id="arJumlah" style="margin: 0; font-size: 32px;">
						<?php echo $jumlah_prov+$jumlah_kota+$jumlah_kab ?>
					</h3>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box" style="cursor: pointer;" onclick="showMark('PROV')">
				<div class="info-box-icon bg-yellow">
					<i class="fa fa-map-marker" style="margin-top: 20px"></i>
				</div>
				<div class="info-box-content">
					<h4>DPR-RI</h4>
					<h3 id="arProvinsi" style="margin: 0; font-size: 32px;">{{ $jumlah_prov }}</h3>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box" style="cursor: pointer;" onclick="showMark('KOTA')">
				<div class="info-box-icon bg-yellow">
					<i class="fa fa-map-marker" style="margin-top: 20px"></i>
				</div>
				<div class="info-box-content">
					<h4>DPRD Tingkat I</h4>
					<h3 id="arKotaData" style="margin: 0; font-size: 32px;">{{ $jumlah_kota }}</h3>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box" style="cursor: pointer;" onclick="showMark('KAB')">
				<div class="info-box-icon bg-yellow">
					<i class="fa fa-map-marker" style="margin-top: 20px"></i>
				</div>
				<div class="info-box-content">
					<h4>DPRD Tingkat II</h4>
					<h3 id="arKabData" style="margin: 0; font-size: 32px;">{{ $jumlah_kab }}</h3>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<div class="info-box-icon bg-yellow">
					<i class="fa fa-user" style="margin-top: 20px"></i>
				</div>
				<div class="info-box-content">
					<h4>Bakal Caleg</h4>
					<h3 id="arBalonData" style="margin: 0; font-size: 32px;">0</h3>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<div class="info-box-icon bg-yellow">
					<i class="fa fa-file" style="margin-top: 20px"></i>
				</div>
				<div class="info-box-content">
					<h4>SK</h4>
					<h3 id="arSKData" style="margin: 0; font-size: 32px;">0</h3>
				</div>
			</div>
		</div>
	</div>
	<div id="legend"></div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ4ajEH8KX_AjJBaU2cxe-LFzIEgn90uI&callback=initMap"
    async defer></script>
<script>
	var map, lat, long, infoWindowProv
	var marker_prov_n = []
	var marker_kota_n = []
	var marker_kab_n = []
	var m_prov = "{{ asset('/asset/icon/user-black-green.png') }}";
	var m_kota = "{{ asset('/asset/icon/user-green.png') }}";
	var m_kab  = "{{ asset('/asset/icon/user-green-black.png') }}";
	
	$(document).ready(function(){
		setMaps(map);
	})

	var pos_prov = [
	@foreach($dataProvP as $prov)
	["{{ $prov->geo_prov_nama }}", {{ $prov->geo_prov_lat }}, {{ $prov->geo_prov_lng }}],
	@endforeach
	]

	var pos_kota = [
	@foreach($dataKotaP as $kota)
	["{{ $kota->geo_prov_nama }}", {{ $kota->geo_prov_lat }}, {{ $kota->geo_prov_lng }}],
	@endforeach
	]

	var pos_kab = [
	@foreach($dataKabP as $kab)
	["{{ $kab->geo_kab_nama }}", {{ $kab->geo_kab_lat }}, {{ $kab->geo_kab_lng }}],
	@endforeach
	]

	function initMap(){
		map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: -1.594185, lng: 119.827881},
		  zoom: 5
		});

		for(var a = 0; a < pos_prov.length; a++){
			addMarkerProvinsi(m_prov, pos_prov[a][1], pos_prov[a][2], pos_prov[a][0])
		}

		for(var b = 0; b < pos_kota.length; b++){
			addMarkerKota(m_kota, pos_kota[b][1], pos_kota[b][2], pos_kota[b][0])
		}

		for(var c = 0; c < pos_kab.length; c++){
			addMarkerKabupaten(m_kab, pos_kab[c][1], pos_kab[c][2], pos_kab[c][0])
		}
		  var legend = document.getElementById("legend");
          var name = "DPR-RI";
          var icon = m_prov;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + m_prov + '"> ' + name;
          legend.appendChild(div);

          var name = "DPRD Tingkat I";
          var icon = m_kota;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + m_kota + '"> ' + name;
          legend.appendChild(div);

          var name = "DPRD Tingkat II";
          var icon = m_kab;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + m_kab + '"> ' + name;
          legend.appendChild(div);

          map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);
	}

	$("#arProv").on('change', function(){
		var prov = $("#arProv").val();

		changeKabOption('#arProv', '#arKokab', prov);
	});

	function getDataPilkada(){
		var jenkada = $("#jenkada").val();
		var prov = $("#arProv").val();
		var kota = $("#arKokab").val();
		var year = $("#arYear").val();

		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxGetDataPilkada') }}",
			data : {
				'jenkada' : jenkada,
				'prov' : prov, 
				'kota' : kota,
				'year' : year
			},
			dataType : "json",
			success:function(get){

			}
		});
	}
function addMarkerProvinsi(icon, lat, long, name){
	var content = "<div id='content'>"+
				"<h5><small>Penyelenggara</small><br>Provinsi "+name+"</h5>"+
				"</div>";
	infoWindowProv = new google.maps.InfoWindow();
	var marker_prov = new google.maps.Marker({
				position : {lat:lat, lng:long},
				map : map,
				icon: icon,
				title : name,
				size : new google.maps.Size(50, 50)
			});
	google.maps.event.addListener(marker_prov, 'click', infoCallback(content, marker_prov));
	marker_prov_n.push(marker_prov);
	setMapsProv(map);
	/*console.log(marker_prov_n.length);*/
}
function addMarkerKota(icon, lat, long, name){
	var content = "<div id='content'>"+
				"<h5><small>Penyelenggara</small><br>"+name+"</h5>"+
				"</div>";
	infoWindowProv = new google.maps.InfoWindow();
	var marker_kota = new google.maps.Marker({
				position : {lat:lat, lng:long},
				map : map,
				icon: icon,
				title : name,
				size : new google.maps.Size(50, 50)
			});
	google.maps.event.addListener(marker_kota, 'click', infoCallback(content, marker_kota));
	marker_kota_n.push(marker_kota);
	setMapsKota(map);
	/*console.log(marker_prov_n.length);*/
}
function addMarkerKabupaten(icon, lat, long, name){
	var content = "<div id='content'>"+
				"<h5><small>Penyelenggara</small><br>Kabupaten "+name+"</h5>"+
				"</div>";
	infoWindowProv = new google.maps.InfoWindow();
	var marker_kab = new google.maps.Marker({
				position : {lat:lat, lng:long},
				map : map,
				icon: icon,
				title : name,
				size : new google.maps.Size(50, 50)
			});
	google.maps.event.addListener(marker_kab, 'click', infoCallback(content, marker_kab));
	marker_kab_n.push(marker_kab);
	setMapsKab(map);
	/*console.log(marker_prov_n.length);*/
}
function infoCallback(over, marker) {
	  return function() {
		infoWindowProv.close();
		// update the content of the infowindow before opening it
		infoWindowProv.setContent(over)
		infoWindowProv.open(map, marker);

	  };
	}
function setMapsProv(map){ 
	for(var q = 0; q < marker_prov_n.length; q++){
		marker_prov_n[q].setMap(map);
	}
}
function setMapsKota(map){ 
	for(var q = 0; q < marker_kota_n.length; q++){
		marker_kota_n[q].setMap(map);
	}
}
function setMapsKab(map){ 
	for(var q = 0; q < marker_kab_n.length; q++){
		marker_kab_n[q].setMap(map);
	}
}
function showMark(key){
	if(key == "ALL"){
		setMapsKab(map);
		setMapsKota(map);
		setMapsProv(map);
	}else if(key == "PROV"){
		setMapsKab(null);
		setMapsKota(null);
		setMapsProv(map);
	}else if(key == "KOTA"){
		setMapsKab(null);
		setMapsKota(map);
		setMapsProv(null);
	}else{
		setMapsKab(map);
		setMapsKota(null);
		setMapsProv(null);
	}
}
</script>
@endsection