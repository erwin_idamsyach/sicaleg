@extends('main.layout.layout')
@section('title-page', 'Agenda - SICALEG')

@section('content')
<section class="content-header">
	<h1>
		Agenda
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>		
		<li><a href="#"><i class="fa fa-calendar"></i> Agenda</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8">
					<h4>Agenda</h4>
				</div>
				<div class="col-md-4">
					<button class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalAgenda">
						<i class="fa fa-plus"></i> Tambah Agenda
					</button>
				</div>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-hover table-bordered">
				<thead>
					<tr>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">No</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">Kegiatan</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">Tempat</th>
						<th class="text-center" colspan="2" style="vertical-align: middle;">Jadwal</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">Deskripsi</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">Status</th>
					</tr>
					<tr>
						<th class="text-center" style="vertical-align: middle;">Mulai</th>
						<th class="text-center" style="vertical-align: middle;">Selesai</th>
					</tr>
				</thead>
				<tbody id="area-agenda">
					<?php 
					$a = 1; 
					$date   = strtotime($date);
					?>
					@foreach($dataAgenda as $get)
					<tr>
						<td style="vertical-align: middle;"><?php echo $a++; ?></td>
						<td style="vertical-align: middle;">{{ $get->kegiatan }}</td>
						<td style="vertical-align: middle;">{{ $get->tempat }}</td>
						<td class="text-center" style="vertical-align: middle;">
							<?php 
							if($get->waktu_mulai == null){
								$date_s = strtotime(date("Y-m-d"));
								echo "-";
							}else{
								$date_s = strtotime($get->waktu_mulai);
								$date_ns = date("d M Y", $date_s);
								echo $date_ns;
							}
							/*echo $date_s." ".$date_e;*/
							$dates = date("d M Y", $date);
							?>
						</td>
						<td class="text-center" style="vertical-align: middle;">
							<?php 
							if($get->waktu_selesai == null){
								$date_e = strtotime(date("Y-m-d"));
								echo "-";
							}else{
								$date_e = strtotime($get->waktu_selesai);
								$date_ne = date("d M Y", $date_e);
							}
							echo $date_ne;
							?>
						</td>
						<td style="vertical-align: middle;"><p><?php echo $get->deskripsi ?></p></td>
						<td class="text-right"  style="vertical-align: middle;">
							<?php
							if($date >= $date_s && $date <= $date_e){
								?>
								<div class="label label-success label-sm">SEDANG BERLANGSUNG</div>
								<?php
							}elseif($date < $date_s){
								?>
								<div class="label label-warning label-sm">BELUM AKTIF</div>
								<?php
							}else{
								?>
								<div class="label label-danger label-sm">TELAH USAI</div>
								<?php
							}
							?>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modalAgenda">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header clr-warning">
					<button class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="area-title">Add Agenda</h4>
				</div>
				<div class="modal-body">
					<form action="{{ asset('agenda/add_action') }}" method="get">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<div class="row">
								<label class="col-md-3 col-sm-4 col-xs-12">Nama Agenda</label>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<input type="text" class="form-control" name="inp_agenda">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<label class="col-md-3 col-sm-4 col-xs-12">Pelaksana</label>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<input type="text" class="form-control" name="inp_pelaksana">
								</div>
							</div>
						</div>						
						<div class="form-group">
							<div class="row">
								<label class="col-md-3 col-sm-4 col-xs-12">Tempat</label>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<input type="text" class="form-control" name="inp_tempat">
								</div>	
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<label class="col-md-3 col-sm-4 col-xs-12">Waktu Mulai</label>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<input type="date" class="form-control" name="inp_date_start">
								</div>	
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<label class="col-md-3 col-sm-4 col-xs-12">Waktu Selesai</label>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<input type="date" class="form-control" name="inp_date_end">
								</div>	
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<label class="col-md-3 col-sm-4 col-xs-12">Deskripsi</label>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<textarea name="inp_deskripsi" class="form-control"></textarea>
								</div>	
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-sm-4 col-xs-12"></div>
							<div class="col-md-9 col-sm-8 col-xs-12">
								<button class="btn btn-warning">SUBMIT</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection