@extends('main.layout.layout')
@section('title-page', 'Tambah Agenda - SICALEG')
@section('content')
<section class="content-header">
	<h1>
		Agenda
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>		
		<li><a href="#"><i class="fa fa-calendar"></i> Agenda</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header"></div>
		<div class="box-body">
			<form action="{{ asset('agenda/add_action_n') }}" method="get">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button class="btn btn-warning" type="button" onclick="appendElement()">Tambah Sub-Agenda</button>
				<hr>
				<table class="table" id="table-sub-agenda">
					<thead>
						<th class="text-center" style="width: 40%">Agenda</th>
						<th class="text-center" style="width: 10%">Waktu Mulai</th>
						<th class="text-center" style="width: 10%">Waktu Selesai</th>
						<th class="text-center" style="width: 60%">Deskripsi</th>
					</thead>
					<tbody id="area-append-el">
						
					</tbody>
				</table>
			</form>
		</div>
	</div>
</section>
<script>
	$(document).ready(function(){
		$('table').DataTable().destroy();
		$("#table-sub-agenda").hide();
	});
	function appendElement(){
		$("#table-sub-agenda").show();
		$("#area-append-el").append('<tr>'+
				'<td><input type="text" class="form-control" name="inp_agenda[]"></td>'+
				'<td><input type="date" class="form-control" name="inp_date_start[]"></td>'+
				'<td><input type="date" class="form-control" name="inp_date_end[]"></td>'+
				'<td><textarea class="form-control" name="inp_date_start[]"></textarea></td>'+
				'</tr>');
	}
</script>
@endsection