@extends('main.layout.layout-front')
@section('title-page', 'PENDAFTARAN BACALEG PARTAI HANURA')

@section('content')
<div class="text-block">


			
	<div class="container">
   		<div class="row">
   	


		<div class="col-md-2 col-sm-2 col-xs-4 img-container">

				<br class="hidden-md hidden-lg">

			<img class="img-responsive hidden-sm hidden-xs" src="asset/img/partai/hanura.jpg">


			<img src="asset/img/partai/hanura.jpg" align="center" class="img-responsive hidden-md hidden-lg">

		</div>

		<div class="col-md-10 col-sm-10 col-xs-7">
			

			<h1 class="text-left hidden-sm hidden-xs">
				PENDAFTARAN BAKAL CALON LEGISLATIF PARTAI HANURA
				<br>
				<small>
					untuk PILEG 2019
				</small>
			</h1>
			<h4 style="color: #000;" class="text-left hidden-md hidden-lg col-xs-11">
				PENDAFTARAN BAKAL CALON LEGISLATIF PARTAI HANURA
				<br>
				<small style="color: #fff;">
				untuk PILEG 2019
				</small>
			</h4>
   			</div>

   		</div>
	</div>
	


	
</div><br><br>
@if(session('error'))
<div class="alert alert-danger"><h4>Data sudah ada di database.</h4></div>
@else
@endif
<form action="{{ asset('insertToDB') }}" method="post" id="form-biodata" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="container">
	<div class="panel panel-default" id="panel-1">
		<div class="panel-body">
			<div class="row form-group hidden-sm hidden-xs" style="margin-bottom: 0;">
		        <div class="col-xs-12">
		            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
		                <li class="active"><a href="#step-1">
		                	<div class="row">
		                		<div class="col-md-1 no-padding text-center">
		                			<h4><b>1</b></h4>
		                		</div>
		                		<div class="col-md-11 no-padding text-left">
				                    <h4 class="list-group-item-heading">LANGKAH PERTAMA</h4>
				                    <p class="list-group-item-text"><b>FORMULIR BIODATA</b></p>
		                		</div>
		                	</div>
		                </a></li>
		                <li class="disabled"><a href="#step-2">
		                	<div class="row">
		                		<div class="col-md-1 no-padding text-center">
		                			<h4><b>2</b></h4>
		                		</div>
		                		<div class="col-md-11 no-padding text-left">
				                    <h4 class="list-group-item-heading">LANGKAH KEDUA</h4>
				                    <p class="list-group-item-text"><b>FORMULIR RIWAYAT HIDUP</b></p>
		                		</div>
		                	</div>
		                </a></li>
		            </ul>
		        </div>
			</div>
			<b> <i class="red">*</i> WAJIB DIISI</b>
			<!-- <hr> -->
			<div id="phase-1">
			<div class="sub-title">
				- KATEGORI CALON LEGISLATIF
		

			

			</div>

				<div class="form-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">
							Kategori Pemilihan
						</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<select name="kategori_caleg" id="kategori_caleg" class="form-control" required="" onchange="getDataDapil()">
								<option value="" selected="" disabled="">--Pilih Kategori--</option>
								@foreach($dataTingkat as $kat)
								<option value="{{ $kat->id }}">{{ $kat->tingkat_caleg }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			<div id="dapil-nasional">
				<div class="form-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">Daerah Pemilihan</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
						
							<select class="form-control" name="dapil" id="dapil-nasionals">
								<option value="" selected="" disabled="">--Pilih DAPIL--</option>
								@foreach($dataDapilRi as $ri)
								<option value="{{ $ri->dapil_id }}">{{ $ri->nama_dapil }}</option>
								@endforeach
							</select>

						</div>

						<div id="dapil-nasional-2" class="col-md-3 col-sm-6 col-xs-12">
						
							<select class="form-control" name="dapil2" id="dapil-nasional">
								<option value="" selected="" disabled="">--Pilih DAPIL--</option>
								@foreach($dataDapilRi as $ri)
								<option value="{{ $ri->dapil_id }}">{{ $ri->nama_dapil }}</option>
								@endforeach
							</select>

						</div>

						<div class="col-md-2 col-sm-6 col-xs-12">
							
							<button type="button" id="tambah" name="tambah" class="btn btn-primary" onclick="getDataDapil2()">+</button>
							<button type="button" id="min" name="min" class="btn btn-danger" onclick="getDataDapil2_X()">-</button>

						</div>
						

					</div>

					</div>
				</div>
			</div>

		

			<div id="dapil-provinsi">
				<div class="form-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">Provinsi</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<select name="prov" id="prov-1" class="form-control" onchange="getDapilProv()">
								<option value="" selected="" disabled="">--Pilih Provinsi--</option>
								@foreach($dataProv as $prov)
								<option value="{{ $prov->geo_prov_id }}">{{ $prov->geo_prov_nama }}</option>
								@endforeach
							</select>

				
						</div>
						<div id="dapil-provinsi-2" class="col-md-3 col-sm-6 col-xs-12">
							<select name="prov-2" id="prov-12" class="form-control" onchange="getDapilProv2()">
								<option value="" selected="" disabled="">--Pilih Provinsi--</option>
								@foreach($dataProv as $prov)
								<option value="{{ $prov->geo_prov_id }}">{{ $prov->geo_prov_nama }}</option>
								@endforeach
							</select>

				
						</div>


						<div class="col-md-2 col-sm-6 col-xs-12">
							
							<button type="button" id="tambah" name="tambah" class="btn btn-primary" onclick="getDataDapil3()">+</button>
							<button type="button" id="min" name="min" class="btn btn-danger hidden-xs hidden-sm" onclick="getDataDapil3_X()">-</button>

						</div>
						


						

					</div>
				</div>
				<div class="from-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">Daerah Pemilihan</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<select name="dapil_prov" id="dapil_prov" class="form-control">
								<option value="" selected="" disabled="">--Pilih DAPIL--</option>
							</select>
					

						</div>

						<div id="dapil-09" class="col-md-3 col-sm-6 col-xs-12">
							<select name="dapil_kotane" id="dapil_kotane" class="form-control">
								<option value="" selected="" disabled="">--Pilih DAPIL--</option>
							</select>
							
							<button type="button" id="min" name="min" class="btn btn-danger hidden-lg hidden-md" onclick="getDataDapil3_X()">-</button>		

						</div>

						

					</div>
				</div>
			</div>
			<div id="dapil-kota">
				<div class="form-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">Provinsi</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<select name="prov" id="prov-2" class="form-control" onchange="getDKota()">
								<option value="" selected disabled>--Pilih Provinsi--</option>
								@foreach($dataProv as $prov)
								<option value="{{ $prov->geo_prov_id }}">{{ $prov->geo_prov_nama }}</option>
								@endforeach
							</select>
						
						</div>

						<div id="province" class="col-md-3 col-sm-6 col-xs-12">
							<select name="prov-3" id="prov-3" class="form-control" onchange="getDKota2()">
								<option value="" selected disabled>--Pilih Provinsi--</option>
								@foreach($dataProv as $prov)
								<option value="{{ $prov->geo_prov_id }}">{{ $prov->geo_prov_nama }}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12">
						

							<button type="button" id="tambah" name="tambah" class="btn btn-primary" onclick="getDataDapil6()">+</button>
							<button type="button" id="min" name="min" class="btn btn-danger hidden-sm hidden-xs" onclick="getDataDapil6_X()">-</button>


						</div>
						

					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">Kota / Kabupaten</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<select name="" id="kota" class="form-control" onchange="getDapilKota()">
								<option value="" selected disabled>--Pilih Kota--</option>
							</select>
						</div>
			

						<div id="kota_class" class="col-md-3 col-sm-6 col-xs-12">
							<select name="" id="kota_2" class="form-control" onchange="getDapilKota2()">
								<option value="" selected disabled>--Pilih Kota--</option>
							</select>
						</div>

					
						
							
							

						

					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-md-2 col-sm-6 col-xs-12">Daerah Pemilihan</label>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<select name="dapil_kota" id="dapil_kota" class="form-control">
								<option value="" selected disabled>--Pilih DAPIL--</option>
							</select>
						</div>
						
						<div id="dapilk" class="col-md-3 col-sm-6 col-xs-12">
							<select name="dapil_kota7" id="dapil_kota7" class="form-control">
								<option value="" selected disabled>--Pilih DAPIL--</option>
							</select>
							<button type="button" id="min" name="min" class="btn btn-danger hidden-lg hidden-md" onclick="getDataDapil6_X()">-</button>

						</div>


					
						

					</div>
				</div>
			</div>
			<input type="hidden" name="jenis_caleg" value="">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					
					<div class="sub-title">
						- DATA DIRI
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Identitas </label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<select class="form-control" id="identitas" name="identitas" onchange="getIDType()" required>
									<option value="">--- Pilih Identitas ---</option>
									<option value="KTP">KTP</option>
									<option value="SIM">SIM</option>
									<option value="Paspor">PASPOR</option>
									<option value="NPWP">NPWP</option>
									<option value="KK">KK</option>
								</select><span><i class="red">*</i></span>
							</div>	

							<div class="col-md-3 col-sm-6 col-xs-12 multi-row">
								<input type="text" class="form-control" id="noIdentitas" name="noIdentitas" placeholder="Nomer Identitas" required="" /><span><i class="red">*</i></span>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12 multi-row" id="responseCheck"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputEmail3" class="col-md-2 col-sm-2 col-xs-12">
								Nama
							</label>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<input type="text" name="namaDepan" class="form-control" id="namaDepan" placeholder="Nama Depan " required><span><i class="red">*</i></span>
							</div></span>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<input type="text" name="namaTengah" class="form-control" id="namaTengah" placeholder="Nama Tengah " required><span><i class="red">*</i></span>
							</div></span>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<input type="text" name="namaBelakang" class="form-control" id="namaBelakang" placeholder="Nama Belakang " required><span><i class="red">*</i></span>
							</div></span>
						</div>
					</div> 
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Gelar</label>
							<div class="col-md-10 col-sm-6 col-xs-12">
								<div class="row">
									<div class="col-md-2 col-sm-4 col-xs-12">
										<label>Depan</label>
										<input type="text" name="gelar_depan" class="form-control">
									</div>
									<div class="col-md-2 col-sm-4 col-xs-12">
										<label>Belakang</label>
										<input type="text" name="gelar_belakang" class="form-control">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Tempat/Tanggal Lahir</label></div>
							<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
								<select class="form-control" id="tlProv" name="tlProv" required onchange="getKotaLahir()">
									<option value="0">--- Pilih Provinsi ---</option>
									@foreach($dataProv as $prov)
									<option value="{{ $prov->geo_prov_id }}">{{ $prov->geo_prov_nama }}</option>
									@endforeach
								</select><span><i class="red">*</i></span>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
								<select class="form-control" id="tlKab" name="tempatLahir" required>
									<option value="0" >--- Pilih Kota/Kabupaten ---</option>
								</select><span><i class="red">*</i></span>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
								<input type="date" name="tanggalLahir" class="form-control" id="datepicker" placeholder="dd-mm-yyyy" required><span><i class="red">*</i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label for="inputPassword3" class="col-md-2 col-sm-4 col-xs-12">Agama</label>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<select class="form-control" id="agama" name="agama" required>
									<option value="">--- Pilih ---</option>
										<option value="Islam">Islam</option>
										<option value="Kristen">Kristen</option>
										<option value="Katolik">Katolik</option>
										<option value="Hindu">Hindu</option>
										<option value="Budha">Budha</option>
										<option value="Konghucu">Konghucu</option>
								</select><span><i class="red">*</i></span>
							</div>
							<label for="inputPassword3" class="col-md-3 col-sm-3 col-xs-12 text-right">Jenis Kelamin </label>
							<div class="col-md-3 col-sm-3 col-xs-12">
									<input type="radio" name="jenisKelamin" value="Pria">Pria</input>
									<input type="radio" name="jenisKelamin" value="Wanita">Wanita</input><span><i class="red">*</i></span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">

						<label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Alamat </label>

							<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
								<select class="form-control" id="abProv" name="abProv" required onchange="getEKota()">
									<option value="">--- Pilih Provinsi ---</option>
									@foreach($dataProv as $prov)
									<option value="{{ $prov->geo_prov_id }}">{{ $prov->geo_prov_nama }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
								<select class="form-control" id="abKab" name="abKab" required onchange="getKecamatan()">
									<option value="" >--- Pilih Kota/Kabupaten ---</option>
								</select>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
								<select class="form-control" id="abKec" name="abKec" required onchange="getKelurahan()">
									<option value="">--- Pilih Kecamatan ---</option>
								</select>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
								<select class="form-control" id="abKel" name="abKel">
									<option value="">--- Pilih Kelurahan ---</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
						<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
							
						</div>
							
							<div class="col-md-9 col-sm-9 col-xs-10">
								<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Pilih Lokasi Pada Maps" required><span><i class="red">*</i></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0;">
								<label for="inputEmail3" class="col-md-2 col-sm-6 col-xs-12">Status Pernikahan</label>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<select name="statusPernikahan" id="statusPernikahan" class="form-control" onchange="disableMenikah();" required>
										<option value="">--- Pilih ---</option>
											<option value="belum">Belum Menikah</option>
											<option value="menikah">Menikah</option>
											<option value="Janda/Duda">Janda/Duda</option>
									</select><span><i class="red">*</i></span>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<input type="text" class="form-control" name="namaPasangan" id="namaPasangan" placeholder="Nama Pasangan" disabled />
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<input type="text" class="form-control" name="jumlahAnak" id="jumlahAnak" placeholder="Jumlah Anak" disabled />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">No. Telpon</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="number" name="telpon" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">No. Handphone</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="number" name="handphone" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Email</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="email" name="email" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Twitter</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="text" name="twitter" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Facebook</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="text" name="facebook" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Foto</label>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="file" class="form-control" name="foto">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Jabatan di HANURA</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="text" name="jabatan_hanura" class="form-control">
							</div>
							<div class="col-md-3 col-sm-1">
			        			<button type="button" class="btn btn-primary" onclick="tampil1()" id="jabatan_hanura_btn">+</button>
			
			        		</div>
						</div>
						<div class="row">
					
					<div class="col-md-2 col-sm-6 col-xs-12">
						
					</div>

						<div id="jabtanya1" class="col-md-3 col-sm-6 col-xs-12">
								<input type="text" name="jabatan_hanura2" id="jabatan_hanura2" class="form-control">
							</div>

							<div id="buttone" class="col-md-3 col-sm-1">
			        			<button type="button" class="btn btn-danger" onclick="hapus1()" id="jabatan_hanura_btn2">-</button>
			        		</div>
							
						</div>

						<input type="hidden" value="1" id="jml_jbtHanura" name="jml_jbtHanura">
					</div>
					<div class="form-group">
						<div class="row">
							<label class="col-md-2 col-sm-6 col-xs-12">Jabatan diluar HANURA</label>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<input type="text" name="jabatan_diluar_hanura" class="form-control">
							</div>
							
							<div class="col-md-3 col-sm-1">
			        			<button type="button" class="btn btn-primary" onclick="tampil2()" id="jabatan_hanura_btn">+</button>
			        	
			        		</div>
						</div>
						<div class="row">
							
							<div class="col-md-2 col-sm-6 col-xs-12">
								
							</div>

							<div id="jabtanya2" class="col-md-3 col-sm-6 col-xs-12">
								<input type="text" name="jabatan_diluar_hanura2"
								id="jabatan_diluar_hanura2" class="form-control">
							</div>

							<div id="hapuse" class="col-md-3 col-sm-1">
			        
			        			<button type="button" class="btn btn-danger" onclick="hapus2()" id="jabatan_hanura_btn2">-</button>
			        		</div>

						</div>
					</div>	
					<div class="form-group">
						<div class="col-md-12">
							<button class="btn btn-warning pull-right" type="button" onclick="goTo('panel-2')">BERIKUTNYA</button>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default" id="panel-2">
		<div class="panel-body">
			<div class="row form-group hidden-sm hidden-xs" style="margin-bottom: 0;">
		        <div class="col-xs-12">
		            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
		                <li><a href="#" onclick="goTo('panel-1')">
		                	<div class="row">
		                		<div class="col-md-1 no-padding text-center">
		                			<h4><b>1</b></h4>
		                		</div>
		                		<div class="col-md-11 no-padding text-left">
				                    <h4 class="list-group-item-heading">LANGKAH PERTAMA</h4>
				                    <p class="list-group-item-text"><b>FORMULIR BIODATA</b></p>
		                		</div>
		                	</div>
		                </a></li>
		                <li class="active"><a href="#step-2">
		                	<div class="row">
		                		<div class="col-md-1 no-padding text-center">
		                			<h4><b>2</b></h4>
		                		</div>
		                		<div class="col-md-11 no-padding text-left">
				                    <h4 class="list-group-item-heading">LANGKAH KEDUA</h4>
				                    <p class="list-group-item-text"><b>FORMULIR RIWAYAT HIDUP</b></p>
		                		</div>
		                	</div>
		                </a></li>
		            </ul>
		        </div>
			</div>
			<div class="box box-primary">
				<div class="sub-title">
					<b>Pendidikan</b>
				</div>
				<div class="box-body" id="pendidikan">
					<form action="" method="get" id="form-riwayat">
					<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">
			    		<div class="row">
			    			<div class="col-md-2 col-sm-4 col-xs-12">
			    				<div class="form-group">
			              			<input type="text" class="form-control pdk_thn" id="pendidikan_tahun1" name="pdk_tahun[]" placeholder="Tahun">
			            		</div>
			    			</div>	
			        		<div class="col-md-4 col-sm-7 col-xs-12">
			        			<div class="form-group">
			                      	<input type="text" class="form-control pdk_desc" id="pendidikan_keterangan1" name="pdk_desc[]" placeholder="Keterangan" style="width: 420px;">
			                    </div>
			        		</div>	
			        		<div class="col-md-1 col-sm-1">
			        			<button type="button" class="btn btn-primary" id="add_pendidikan">+</button>
			        		</div>
			    		</div>	
					</div>
				</div>
				<input type="hidden" value="1" id="jml_pendidikan" name="jml_pendidikan">
			</div>
			<div class="box box-primary">
				<div class="sub-title">
					<b>Organisasi</b>
				</div>
				<div class="box-body" id="organisasi">
					<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">
			    		<div class="row">
			    			<div class="col-md-2 col-sm-4 col-xs-12">
			    				<div class="form-group">
			              			<input type="text" class="form-control" id="organisasi_tahun1" name="org_tahun[]" placeholder="Tahun">
			            		</div>
			    			</div>
			        		<div class="col-md-4 col-sm-7 col-xs-12">
			        			<div class="form-group">
			                      	<input type="text" class="form-control" id="organisasi_keterangan1" name="org_desc[]" placeholder="Keterangan"  style="width: 420px;">
			                    </div>
			        		</div>
			        		<div class="col-md-1 col-sm-1 col-xs-1">
			        			<button type="button" class="btn btn-primary" id="add_organisasi">+</button>
			        		</div>
			    		</div>
					</div>
				</div>
				<input type="hidden" value="1" id="jml_organisasi" name="jml_organisasi">
			</div>
			<div class="box box-primary">
				<div class="sub-title">
					<b>Pekerjaan</b>
				</div>
				<div class="box-body" id="pekerjaan">
					<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pekerjaan">
			    		<div class="row">
			    			<div class="col-md-2 col-sm-4 col-xs-12">
			        			<div class="form-group">
			              			<input type="text" class="form-control" id="pekerjaan_tahun1" name="pkj_tahun[]" placeholder="Tahun">
			            		</div>
			        		</div>
			        		<div class="col-md-4 col-sm-7 col-xs-12">
			        			<div class="form-group">
			                      	<input type="text" class="form-control" id="pekerjaan_keterangan1" name="pkj_desc[]" placeholder="Keterangan" style="width: 420px;">
			                    </div>
			        		</div>
			        		<div class="col-md-1 col-sm-1 col-xs-1">
			        			<button type="button" class="btn btn-primary" id="add_pekerjaan">+</button>
			        		</div>
			    		</div>	
					</div>
				</div>
				<input type="hidden" value="1" id="jml_pekerjaan" name="jml_pekerjaan">
			</div>
			<div class="box box-primary">
				<div class="sub-title">
					<b>Diklat</b>
				</div>
				<div class="box-body " id="diklat">
					<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_diklat">
			    		<div class="row">
			    			<div class="col-md-2 col-sm-4 col-xs-12">
			        			<div class="form-group">
			              			<input type="text" class="form-control" id="diklat_tahun1" name="diklat_tahun[]" placeholder="Tahun">
			            		</div>
			        		</div>
			        		<div class="col-md-4 col-sm-7 col-xs-12">
			        			<div class="form-group">
			                      	<input type="text" class="form-control" id="diklat_keterangan1" name="diklat_desc[]" placeholder="Keterangan" style="width: 420px;">
			                    </div>
			        		</div>
			        		<div class="col-md-1 col-sm-1 col-xs-1">
			        			<button type="button" class="btn btn-primary" id="add_diklat">+</button>
			        		</div>
			    		</div>	
					</div>
				</div>
				<input type="hidden" value="1" id="jml_diklat" name="jml_diklat">
			</div>
			<div class="box box-primary">
				<div class="sub-title">
					<b>Perjuangan</b>
				</div>
				<div class="box-body" id="perjuangan">
					<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">
			    		<div class="row">
			    			<div class="col-md-2 col-sm-4 col-xs-12">
			        			<div class="form-group">
			              			<input type="text" class="form-control" id="perjuangan_tahun1" name="juang_tahun[]" placeholder="Tahun">
			            		</div>
			        		</div>
			        		<div class="col-md-4 col-sm-7 col-xs-12">
			        			<div class="form-group">
			                      	<input type="text" class="form-control" id="perjuangan_keterangan1" name="juang_desc[]" placeholder="Keterangan" style="width: 420px;">
			                    </div>
			        		</div>
			        		<div class="col-md-1 col-sm-1 col-xs-1">
			        			<button type="button" class="btn btn-primary" id="add_perjuangan">+</button>
			        		</div>
			    		</div>	
					</div>
				</div>
				<input type="hidden" value="1" id="jml_perjuangan" name="jml_perjuangan">
			</div>
			<div class="box box-primary">
				<div class="sub-title">
					<b>Penghargaan</b>
				</div>
				<div class="box-body" id="penghargaan">
					<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_penghargaan">
			    		<div class="row">
			    			<div class="col-md-2 col-sm-4 col-xs-12">
			        			<div class="form-group">
			              			<input type="text" class="form-control" id="penghargaan_tahun1" name="phg_tahun[]" placeholder="Tahun">
			            		</div>
			        		</div>
			        		<div class="col-md-4 col-sm-7 col-xs-12">
			        			<div class="form-group">
			                      	<input type="text" class="form-control" id="penghargaan_keterangan1" name="phg_desc[]" placeholder="Keterangan" style="width: 420px;">
			                    </div>
			                    </div>
			        	
			        		 <div class="col-md-1 col-sm-1 col-xs-1">
			        			<button type="button" class="btn btn-primary" id="add_penghargaan">+</button>
			        		</div>
			    		</div>	
					</div>
				</div>
				<input type="hidden" value="1" id="jml_penghargaan" name="jml_penghargaan">
			</div>
			<hr>
			<div class="form-group">
				<div class="row">
					<div class="col-md-12">
						<center>
							<div class="g-recaptcha" data-sitekey="6LejSS0UAAAAAGV2V9zDWXvQO1X6NjT-anSHpY6_"></div>
							<button class="btn btn-warning">SIMPAN</button>
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
@endsection

@section('function')
<script>
	$(document).ready(function(){

	
		$("#hapuse").hide();
		$("#buttone").hide();
		$("#jabtanya2").hide();
		$("#jabtanya1").hide();
		$("#dapilk").hide();
		$("#kota_class").hide();
		$("#province").hide();
		$("#dapil-09").hide();
		$("#dapil-provinsi-2").hide();
		$("#dapil-nasional").hide();
		$("#dapil-nasional-2").hide();
		$("#dapil-provinsi").hide();
		$("#dapil-kota-2").hide();
		$("#dapil-kota").hide();
		$("#panel-2").hide();
		$("#panel-3").hide();
		$("#noIdentitas").attr('disabled', true);
	});
	function goTo(el){
		$("#"+el).show();
		$('html, body').animate({
	        scrollTop: $("#"+el).offset().top
	    }, 700);
	}
	function getDataDapil(){
		var kate = $("#kategori_caleg").val();
		/*alert(kate);*/
		if(kate == 1){
			$("#dapil-nasional").show();
			$("#dapil-nasional-2").hide();
			$("#dapil-provinsi").hide();
			$("#dapil-kota").hide();
		}else if(kate == 2){
			$("#dapil-nasional-2").hide();
			$("#dapil-nasional").hide();
			$("#dapil-provinsi").show();
			$("#dapil-kota").hide();
		}else{
			$("#dapil-nasional").hide();
			$("#dapil-provinsi").hide();
			$("#dapil-kota").show();
		}
	}

	function getDataDapil2() {
		
		$("#dapil_prov_2").show();
		$("#dapil-nasional-2").show();
	}

	function tampil1(){
		$("#jabtanya1").show();
		$("#buttone").show();

	}
	function hapus1(){
		$("#buttone").hide();

		$("#jabtanya1").hide();
	}
	function tampil2(){
		$("#hapuse").show();
		$("#jabtanya2").show();
	}
	function hapus2(){
		$("#hapuse").hide();
		$("#jabtanya2").hide();
	}
	function getDataDapil2_X() {
		
		$("#dapil_prov_2").hide();
		$("#dapil-nasional-2").hide();
	}

	function getDataDapil3(){

		$("#dapil-provinsi-2").show();
		$("#dapil-09").show();
	}

	function getDataDapil3_X(){

		$("#dapil-provinsi-2").hide();
		$("#dapil-09").hide();
	}

	function getDataDapil6() {
		$("#province").show();
		$("#kota_class").show();
		$("#dapilk").show();
	}

	function getDataDapil6_X() {
		$("#province").hide();
		$("#kota_class").hide();
		$("#dapilk").hide();
	}




	function getDapilProv(){
		var prov = $("#prov-1").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajax/dapil/prov') }}",
			data : "prov_id="+prov,
			success:function(resp){
				$("#dapil_prov").html(resp);	
			}
		})
	}
	function getDapilProv2(){
		var prov = $("#prov-12").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajax/dapil/prov') }}",
			data : "prov_id="+prov,
			success:function(resp){
				$("#dapil_kotane").html(resp);	
			}
		})
	}
	function getDapilKota(){
		var kota = $("#kota").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajax/dapil/kota') }}",
			data : "kota="+kota,
			success:function(resp){
				$("#dapil_kota").html(resp);
			}
		})
	}
	function getDapilKota2(){
		var kota = $("#kota_2").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajax/dapil/kota') }}",
			data : "kota="+kota,
			success:function(resp){
				$("#dapil_kota7").html(resp);
			}
		})
	}
	function getDKota(){
		var data = $("#prov-2").val();
		getKota("prov-2", "kota", data);
	}
	function getDKota2(){
		var data = $("#prov-3").val();
		getKota("prov-3", "kota_2", data);
	}
	function getEKota(){
		var data = $("#abProv").val();
		getKota("abProv", "abKab", data);
	}
	function getKota(from, to, data){
		var kota = $("#"+from).val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxGetKabupaten') }}",
			data : "key="+data,
			success:function(resp){
				$("#"+to).html(resp);
			}
		});
	}
	function getKecamatan(){
		var data = $("#abKab").val();

		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxKecamatan') }}",
			data: "key="+data,
			success:function(resp){
				$("#abKec").html(resp);
			}
		})
	}

	function getKelurahan(){
		var data = $("#abKec").val();

		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxKelurahan') }}",
			data : "key="+data,
			success:function(resp){
				$("#abKel").html(resp);
			}
		})
	}
	function getKotaLahir(){
		var kota = $("#tlProv").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxGetKabupaten') }}",
			data : "key="+kota,
			success:function(resp){
				$("#tlKab").html(resp);
			}
		});
	}
	function getIDType(){
		var type = $("#identitas").val();
		if(type != ""){
			$("#noIdentitas").removeAttr('disabled');
		}else{
			$("#noIdentitas").attr('disabled', true);
		}
	}

	function disableMenikah() {
		if ($('#statusPernikahan').val() == 'menikah') {
			$('#jumlahAnak').removeAttr('disabled');
			$('#namaPasangan').removeAttr('disabled');
		} else {
			$('#jumlahAnak').attr('disabled', true);
			$('#namaPasangan').attr('disabled', true);
		}
	}




	$("#add_pendidikan").on("click", function(){
	var jml = $("#jml_pendidikan").val();
	jml = parseInt(jml)+1;
	$("#pendidikan").append('<div class="form-inline pend'+jml+'" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">'+
			    		'<div class="row">'+
			    			'<div class="col-md-2 col-sm-4 col-xs-12">'+
			    				'<div class="form-group">'+
			              			'<input type="text" class="form-control" id="pendidikan_tahun'+jml+'" name="pdk_tahun[]" placeholder="Tahun">'+
			            		'</div>'+
			    			'</div>'+
			        		'<div class="col-md-4 col-sm-7 col-xs-12">'+
			        			'<div class="form-group">'+
			                      	'<input type="text" class="form-control" id="pendidikan_keterangan'+jml+'" name="pdk_desc[]" placeholder="Keterangan" style="width: 420px;">'+
			                    '</div>'+
			        		'</div>'+
			        		'<div class="col-md-1 col-sm-1">'+
			        			'<button type="button" class="btn btn-danger" onclick="delPend('+jml+')">X</button>'+
			        		'</div>'+
			    		'</div>	'+
					'</div>');
	$("#jml_pendidikan").val(jml);
});


$("#add_organisasi").on('click', function(){
	var jml = $("#jml_organisasi").val();
	jml = parseInt(jml)+1;
	$("#organisasi").append('<div class="form-inline org'+jml+'" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">'+
			    		'<div class="row">'+
			    			'<div class="col-md-2 col-sm-4 col-xs-12">'+
			    				'<div class="form-group">'+
			              			'<input type="text" class="form-control" id="organisasi_tahun'+jml+'" name="org_tahun[]" placeholder="Tahun">'+
			            		'</div>'+
			    			'</div>'+
			        		'<div class="col-md-4 col-sm-7 col-xs-12">'+
			        			'<div class="form-group">'+
			                      	'<input type="text" class="form-control" id="organisasi_keterangan'+jml+'" name="org_desc[]" placeholder="Keterangan"  style="width: 420px;">'+
			                    '</div>'+
			        		'</div>'+
			        		'<div class="col-md-1 col-sm-1 col-xs-1">'+
			        			'<button type="button" class="btn btn-danger" id="del_organisasi" onclick="delOrg('+jml+')">X</button>'+
			        		'</div>'+
			    		'</div>'+
					'</div>')
	$("#jml_organisasi").val(jml)
})
$("#add_pekerjaan").on("click", function(){
	var jml = $("#jml_pekerjaan").val();
	jml = parseInt(jml)+1
	$("#pekerjaan").append('<div class="form-inline kerja'+jml+'" role="form" style="width: 100%;padding: 5px;" id="form_pekerjaan">'+
			    		'<div class="row">'+
			    			'<div class="col-md-2 col-sm-4 col-xs-12">'+
			        			'<div class="form-group">'+
			              			'<input type="text" class="form-control" id="pekerjaan_tahun1" name="pkj_tahun[]" placeholder="Tahun">'+
			            		'</div>'+
			        		'</div>'+
			        		'<div class="col-md-4 col-sm-7 col-xs-12">'+
			        			'<div class="form-group">'+
			                      	'<input type="text" class="form-control" id="pekerjaan_keterangan1" name="pkj_desc[]" placeholder="Keterangan" style="width: 420px;">'+
			                    '</div>'+
			        		'</div>'+
			        		'<div class="col-md-1 col-sm-1 col-xs-1">'+
			        			'<button type="button" class="btn btn-danger" id="del_pekerjaan" onclick="delKerja('+jml+')">X</button>'+
			        		'</div>'+
			    		'</div>	'+
					'</div>')
	$("#jml_pekerjaan").val(jml)
})
$("#add_diklat").on('click', function(){
	var jml = $("#jml_diklat").val();
	jml = parseInt(jml)+1;

	$("#diklat").append('<div class="form-inline diklat'+jml+'" role="form" style="width: 100%;padding: 5px;" id="form_diklat">'+
			    		'<div class="row">'+
			    			'<div class="col-md-2 col-sm-4 col-xs-12">'+
			        			'<div class="form-group">'+
			              			'<input type="text" class="form-control" id="diklat_tahun1" name="diklat_tahun1" placeholder="Tahun">'+
			            		'</div>'+
			        		'</div>'+
			        		'<div class="col-md-4 col-sm-7 col-xs-12">'+
			        			'<div class="form-group">'+
			                      	'<input type="text" class="form-control" id="diklat_keterangan1" name="diklat_desc[]" placeholder="Keterangan" style="width: 420px;">'+
			                    '</div>'+
			        		'</div>'+
			        		'<div class="col-md-1 col-sm-1 col-xs-1">'+
			        			'<button type="button" class="btn btn-danger" id="del_diklat" onclick="delDiklat('+jml+')">+</button>'+
			        		'</div>'+
			    		'</div>	'+
					'</div>');

	$("#jml_diklat").val(jml)
});
$("#add_perjuangan").on("click", function(){
	var jml = $("#jml_perjuangan").val()
	jml = parseInt(jml)+1;

	$("#perjuangan").append('<div class="form-inline juang'+jml+'" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">'+
			    		'<div class="row">'+
			    			'<div class="col-md-2 col-sm-4 col-xs-12">'+
			        			'<div class="form-group">'+
			              			'<input type="text" class="form-control" id="perjuangan_tahun1" name="juang_tahun[]" placeholder="Tahun">'+
			            		'</div>'+
			        		'</div>'+
			        		'<div class="col-md-4 col-sm-7 col-xs-12">'+
			        			'<div class="form-group">'+
			                      	'<input type="text" class="form-control" id="perjuangan_keterangan1" name="juang_desc[]" placeholder="Keterangan" style="width: 420px;">'+
			                    '</div>'+
			        		'</div>'+
			        		'<div class="col-md-1 col-sm-1 col-xs-1">'+
			        			'<button type="button" class="btn btn-danger" id="del_perjuangan" onclick="delJuang('+jml+')">X</button>'+
			        		'</div>'+
			    		'</div>	'+
					'</div>')
	$("#jml_perjuangan").val(jml)
});
$("#add_penghargaan").on('click', function(){
	var jml = $("#jml_penghargaan").val();
	jml =parseInt(jml)+1;

	$("#penghargaan").append('<div class="form-inline harga'+jml+'" role="form" style="width: 100%;padding: 5px;" id="form_penghargaan">'+
			    		'<div class="row">'+
			    			'<div class="col-md-2 col-sm-4 col-xs-12">'+
			        			'<div class="form-group">'+
			              			'<input type="text" class="form-control" id="penghargaan_tahun1" name="phg_tahun[]" placeholder="Tahun">'+
			            		'</div>'+
			        		'</div>'+
			        		'<div class="col-md-4 col-sm-7 col-xs-12">'+
			        			'<div class="form-group">'+
			                      	'<input type="text" class="form-control" id="penghargaan_keterangan1" name="phg_desc[]" placeholder="Keterangan" style="width: 420px;">'+
			                    '</div>'+
			        		'</div>'+
			        		'<div class="col-md-1 col-sm-1 col-xs-1">'+
			        			'<button type="button" class="btn btn-danger" id="del_penghargaan" onclick="delHarga('+jml+')">X</button>'+
			        		'</div>'+
			    		'</div>	'+
					'</div>');

	$("#jml_penghargaan").val(jml)
})
function delPend(key){
	$(".pend"+key).remove();
}
function delOrg(key){
	$(".org"+key).remove();
}
function delKerja(key){
	$(".kerja"+key).remove()
}
function delDiklat(key){
	$(".diklat"+key).remove();
}
function delJuang(key){
	$(".juang"+key).remove();
}
function delHarga(key){
	$(".harga"+key).remove()
}
</script>
@endsection