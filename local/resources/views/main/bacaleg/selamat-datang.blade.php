<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Selamat Datang</title>
	<link rel="stylesheet" href="{{ asset('asset/bootstrap/css/bootstrap.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('asset/font-awesome/css/font-awesome.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('asset/css/style.css') }}">
</head>
<body>
	<div class="bg">
		<center>
			<img src="{{ asset('asset/img/hanura.jpg') }}" width="350px" class="img-bg">
			<h1>
				Selamat Datang<br>
				<small>Calon Pengemban Amanat Hati Nurani Rakyat</small>
			</h1>
		</center>
		<div class="container">
			<div class="container">
				<p style="text-align: justify;">Terima kasih telah mendaftar menjadi calon pengemban amanat hati nurani rakyat. Mohon aktifkan terus nomor telpon dan email yang anda daftarkan tadi untuk mendapatkan informasi lebih lanjut tentang pencalonan anggota legislatif Partai HANURA.</p>
			</div>
		</div>
		<center>
			<a href="{{ asset('pileg2019') }}" class="btn btn-danger" type="button">KEMBALI KE HALAMAN PENDAFTARAN</a>
		</center>
	</div>
</body>
</html>