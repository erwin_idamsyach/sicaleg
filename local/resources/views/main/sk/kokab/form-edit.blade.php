@extends('main.layout.layout')
@section('title-page', 'Input SK Kota/Kabupaten')

@section('content')
<section class="content-header">
	<h1>
		Input SK
		<small> Balon Tingkat Kota/Kabupaten</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Input SK</a></li>
		<li><a href="#"> Balon Tingkat Kota/Kabupaten</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<h4>Form Input SK</h4>
		</div>
		@foreach($dataBalon as $get)
		@endforeach
		<form action="{{ asset('sk-kokab/edit/action') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate>
		<input type="hidden" name="balon_id" value="{{ $get->id }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">Nama</div>
						<div class="col-md-1 col-sm-1 col-xs-1">:</div>
						<div class="col-md-6 col-sm-6 col-xs-6">{{ $get->nama }}</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">Tempat, Tanggal Lahir</div>
						<div class="col-md-1 col-sm-1 col-xs-1">:</div>
						<div class="col-md-6 col-sm-6 col-xs-6">{{ $get->tempat_lahir.", ".$get->tanggal_lahir }}</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">Jenis Kelamin</div>
						<div class="col-md-1 col-sm-1 col-xs-1">:</div>
						<div class="col-md-6 col-sm-6 col-xs-6">{{ $get->jenis_kelamin }}</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">No. Identitas</div>
						<div class="col-md-1 col-sm-1 col-xs-1">:</div>
						<div class="col-md-6 col-sm-6 col-xs-6">{{ $get->nomer_identitas." / ".$get->jenis_identitas }}</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">Provinsi</div>
						<div class="col-md-1 col-sm-1 col-xs-1">:</div>
						<div class="col-md-6 col-sm-6 col-xs-6">{{ $get->geo_prov_nama }}</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">Kota / Kabupaten</div>
						<div class="col-md-1 col-sm-1 col-xs-1">:</div>
						<div class="col-md-6 col-sm-6 col-xs-6">{{ $get->geo_kab_nama }}</div>
					</div>
				</div>
			</div><hr>
			<table>
				<tr>
					<td>No. SK</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="text" name="no_sk" class="form-control">
					</td>
				</tr>
				<tr>
					<td>Perihal</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="text" name="perihal" class="form-control">
					</td>
				</tr>
				<tr>
					<td>Periode</td>
					<td class="pad">:</td>
					<td>
						<div class="row">
							<div class="col-md-5">
								<select name="periode_1" class="form-control" style="width: 98%">
									<option value="" disabled="" selected="">--Periode--</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
								</select>
							</div>
							<div class="col-md-2">
								<center>
									<b>-</b>
								</center>
							</div>
							<div class="col-md-5">
								<select name="periode_2" class="form-control">
									<option value="" disabled="" selected="">--Periode--</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Sebagai</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="text" class="form-control" name="sebagai">
					</td>
				</tr>
				<tr>
					<td>Tanggal SK</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="date" class="form-control" name="tgl_sk">
					</td>
				</tr>
				<tr>
					<td>No. TPP</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="text" class="form-control" name="no_tpp">
					</td>
				</tr>
				<tr>
					<td>Kategori Daerah</td>
					<td class="pad ">:</td>
					<td class="right">
						<select name="kategori" class="form-control">
							<option value="" selected="" disabled="">--Kategori Daerah--</option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Kader/Non-Kader</td>
					<td class="pad">:</td>
					<td class="right">
						<div class="radio">
							<label>
								<input type="radio" name="kader" value="kader">&nbsp; Kader
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="kader" value="non-kader">&nbsp; Non-Kader
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td>Persyaratan</td>
					<td class="pad">:</td>
					<td class="right">
						<div class="row">
							<div class="col-md-6">
								<input type="number" name="jumlah_kursi" class="form-control">
							</div>
							<div class="col-md-2">Kursi</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Kursi Partai HANURA</td>
					<td class="pad">:</td>
					<td class="right">
						<div class="row">
							<div class="col-md-6">
								<input type="number" name="kursi_hanura" class="form-control">
							</div>
							<div class="col-md-2">Kursi</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Koalisi Partai</td>
					<td class="pad">:</td>
					<td class="right">
						<div class="row" id="area-append">
							<div class="col-md-7">
								<select name="partai[]" class="form-control">
									<option value="" selected disabled>--Pilih Partai--</option>
									@foreach($dataPartai as $data)
									<option value="{{ $data->NAMA_PARTAI }}">{{$data->NAMA_PARTAI}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-3">
								<input type="number" class="form-control" name="kursi[]" placeholder="Jumlah Kursi">
							</div>
							<div class="col-md-2">
								<button class="btn btn-info" onclick="addItem()"><i class="fa fa-plus"></i></button>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<hr>
			Catatan
			<table>
				<tr>
					<td>a.	Tanggal Hasil Rapat Komite Pembahasan Yang dihadiri oleh DPC dan DPD</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="date" class="form-control" name="tgl_rapat">
					</td>
				</tr>
				<tr>
					<td>b.	Tanggal Hasil Rapat Komite Pengambilan keputusan Pasangan Calon</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="date" class="form-control" name="tgl_putusan">
					</td>
				</tr>
				<tr>
					<td>c.	Hasil Survei</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="text" class="form-control" name="hasil_survei">
					</td>
				</tr>
				<tr>
					<td>Upload SK</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="file" class="form-control" name="file_sk">
					</td>
				</tr>
				<tr>
					<td>Upload Hasil Survei</td>
					<td class="pad">:</td>
					<td class="right">
						<input type="file" class="form-control" name="file_survei">
					</td>
				</tr>
			</table>
			<button class="btn btn-warning">SUBMIT</button>
		</div>
		</form>
	</div>
</section>
<script>
	var no = 0;
	function addItem(){
		var pas = no+1;
		$("#area-append").append('<div class="col-md-7 '+pas+'">'+
								'<select name="partai[]" class="form-control">'+
									'<option value="" selected disabled>--Pilih Partai--</option>'+
									'<option value="Nasdem">Nasdem</option>'+
									'<option value="PKB">PKB</option>'+
									'<option value="PKS">PKS</option>'+
									'<option value="PDIP">PDIP</option>'+
									'<option value="Golkar">Golkar</option>'+
									'<option value="Gerindra">Gerindra</option>'+
									'<option value="Demokrat">Demokrat</option>'+
									'<option value="PAN">PAN</option>'+
									'<option value="PPP">PPP</option>'+
									'<option value="Hanura">Hanura</option>'+
									'<option value="PBB">PBB</option>'+
									'<option value="PKPI">PKPI</option>'+
									'<option value="PDA">PDA</option>'+
									'<option value="PNA">PNA</option>'+
									'<option value="PA">PA</option>'+
								'</select>'+
							'</div>'+
							'<div class="col-md-3 '+pas+'">'+
								'<input type="number" class="form-control" name="kursi[]" '+'placeholder="Jumlah Kursi">'+
							'</div>'+
							'<div class="col-md-2 '+pas+'">'+
								'<button class="btn btn-danger" onclick="removeItem('+(no+1)+')"><i class="fa '+'fa-minus"></i></button>'+
							'</div>');
		no=no+1;
	}
	function removeItem(key){
		$("."+key).remove();
	}
</script>
@endsection