@extends('main.layout.layout')
@section('title-page', 'Input SK Kota/Kabupaten')

@section('content')
<section class="content-header">
	<h1>
		Input SK
		<small> Balon Tingkat Kota/Kabupaten</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
		<li><a href="#"><i class="fa fa-edit"></i> Input SK</a></li>
		<li><a href="#"> Balon Tingkat Kota/Kabupaten</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-warning">
		<div class="box-header">
			<div class="row">
				<div class="col-md-8">
					<h4>List Bakal Calon Kota/Kabupaten</h4>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Balon</th>
						<th class="text-center">Nama</th>
						<th class="text-center">Kelengkapan</th>
						<th class="text-center">Survei</th>
						<th class="text-center">Status</th>
						<th class="text-right">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $a = 1; ?>
					@foreach($dataKokab as $get)
					<tr>
						<td class="text-center"><?php echo $a++; ?></td>
						<td>{{ $get->type }}</td>
						<td>{{ $get->nama }}</td>
						<td class="text-center">
							<?php 
							$cek = 0;
							if($get->file_riwayat != "" || $get->file_riwayat != null){
								$cek = $cek+1;
							}

							if($get->file_visi != "" || $get->file_visi != null){
								$cek = $cek+1;
							}

							if($get->file_ktp != "" || $get->file_ktp != null){
								$cek = $cek+1;
							}

							if($get->file_kk != "" || $get->file_kk != null){
								$cek = $cek+1;
							}

							if($get->file_npwp != "" || $get->file_npwp != null){
								$cek = $cek+1;
							}

							if($get->file_ijazah != "" || $get->file_ijazah != null){
								$cek = $cek+1;
							}

							if($get->file_skck != "" || $get->file_skck != null){
								$cek = $cek+1;
							}

							if($get->file_kta != "" || $get->file_kta != null){
								$cek = $cek+1;
							}

							if($get->file_pendaftaran != "" || $get->file_pendaftaran != null){
								$cek = $cek+1;
							}

							if($get->file_komitmen != "" || $get->file_komitmen != null){
								$cek = $cek+1;
							}

							if($get->file_peryataan != "" || $get->file_peryataan != null){
								$cek = $cek+1;
							}

							if($get->file_tidak_pailit != "" || $get->file_tidak_pailit != null){
								$cek = $cek+1;
							}

							if($get->file_nkri != "" || $get->file_nkri != null){
								$cek = $cek+1;
							}

							echo $cek." / 13";
							?>
						</td>
						<td class="text-center">
							<?php
							$percentage = ($get->survei / $get->responder)*100;
							echo $percentage." %";
							?>
						</td>
						<td class="text-center">{{ $get->flag }}</td>
						<td class="text-right">
							<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View" onclick="viewDetail('{{ $get->id }}')"><i class="fa fa-eye"></i></button>&nbsp;&nbsp;
							<a href="{{ asset('sk-kokab/edit/').'/'.$get->id }}" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modal-detail">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detil Bakal Calon / Calon Tingkat Kota/Kabupaten</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Nama</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-nama"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Tempat, Tanggal Lahir</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-ttl"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Jenis Kelamin</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-jenkel"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">No. Identitas</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-id"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Provinsi</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-prov"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Kota / Kabupaten</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-kota">-</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">No. SK</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-sk"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Perihal</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-perihal"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Sebagai</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-sebagai"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">No. TPP</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-tpp"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Kategori Daerah</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-kategori"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Persyaratan</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-syarat-1"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Tanggal SK</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-tgl-sk"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Periode</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-periode">-</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Kade/Non-Kader</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-kader"></div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">Kursi HANURA</div>
								<div class="col-md-1 col-sm-1 col-xs-1">:</div>
								<div class="col-md-6 col-sm-6 col-xs-6" id="a-syarat-2"></div>
							</div>
						</div>
					</div>
					<hr>
					Catatan
					<div class="row">
						<div class="col-md-6">a.	Tanggal Hasil Rapat Komite Pembahasan Yang dihadiri oleh DPC dan DPD</div>
						<div class="col-md-1">:</div>
						<div class="col-md-5" id="a-catat-1"></div>
					</div>
					<div class="row">
						<div class="col-md-6">b.	Tanggal Hasil Rapat Komite Pengambilan keputusan Pasangan Calon</div>
						<div class="col-md-1">:</div>
						<div class="col-md-5" id="a-catat-2"></div>
					</div>
					<div class="row">
						<div class="col-md-6">c.	Hasil Survei</div>
						<div class="col-md-1">:</div>
						<div class="col-md-5" id="a-catat-3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function viewDetail(key){
		$.ajax({
			type : "GET",
			url  : "{{ asset('throw-data') }}",
			data : "key="+key,
			dataType : "json",
			success:function(get){
				$("#modal-detail").modal('show');
				$("#a-nama").text(get['nama']);
				$("#a-ttl").text(get['tempat_lahir']+", "+get['tgl_lahir']);
				$("#a-jenkel").text(get['jenkel']);
				$("#a-id").text(get['no_id']+" / "+get['jenis_id']);
				$("#a-prov").text(get['provinsi']);
				$("#a-kota").text(get['kota']);

				$("#a-sk").text(get['no_sk']);
				$("#a-perihal").text(get['perihal']);
				$("#a-sebagai").text(get['sebagai']);
				$("#a-tpp").text(get['no_tpp']);
				$("#a-kategori").text(get['kategori']);
				$("#a-syarat-1").text(get['syarat_kursi_1']+" Kursi");
				$("#a-syarat-2").text(get['syarat_kursi_2']+" Kursi");
				$("#a-tgl-sk").text(get['tgl_sk']);
				$("#a-periode").text(get['periode']);
				$("#a-kader").text(get['kader']);

				$("#a-catat-1").text(get['tgl_catat_1']);
				$("#a-catat-2").text(get['tgl_catat_2']);
				$("#a-catat-3").text(get['hasil_survei']);
			}
		})
	}
</script>
@endsection