<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;

class ChecklistController extends Controller{
	public function index(){
		$getData = DB::table('caleg_drh')
					->join('caleg_dapil',
							'caleg_drh.id',
							'=',
							'caleg_dapil.caleg_id')
					->join('m_geo_kab_kpu',
							'caleg_drh.tempat_lahir',
							'=',
							'm_geo_kab_kpu.geo_kab_id')
					->join('tingkat_pemilihan',
						'tingkat_pemilihan.id',
						'=',
						'caleg_dapil.tingkat_caleg')
					->select(
						'caleg_drh.*',
						'caleg_dapil.dapil',
						'tingkat_pemilihan.tingkat_caleg'
						)
					->where('tahun', DB::Raw("YEAR(NOW())"))
					->where('flag', 'Balon')
					->get();
		$getProv = DB::table('m_geo_prov_kpu')->get();
		return view('main.checklist.index', array(
			'dataProv'  => $getProv,
			'dataBalon' => $getData,
			'menu' => "checklist"
			));					
	}

	public function edit_dokumen($key){
		$getUserDetail = DB::table('caleg_drh')->where('id',$key)->get();

		return view('main.checklist.form_edit', array(
			'dataUser' => $getUserDetail,
			'key'      => $key,
			'menu'     => "checklist"
			));
	}

	public function action_edit_dokumen($key){
		$arr 	   = array();
		$f_visi	   = Input::file('filevisiMisi');
		$f_ktp	   = Input::file('filefotoCopyKtp');
		$f_kk      = Input::file('filefotoCopyKk');
		$f_npwp    = Input::file('filefotoCopyNpwp');
		$f_ijazah  = Input::file('filefotoCopyIjazah');
		$f_skck    = Input::file('fileskcs');
		$f_kta     = Input::file('filektaPartaiHanura');
		$f_daftar  = Input::file('fileformPendaftaranCalon');
		$f_komit   = Input::file('filekomitmen');
		$f_taqwa   = Input::file('filebertaqwa');
		$f_nkri	   = Input::file('filetinggal');
		$f_riwayat = Input::file('filedaftarRiwayatHidup'); /* POST DARI VIEW */
		
		if(Input::hasFile('filedaftarRiwayatHidup')){
			/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
			$ext = File::extension($f_riwayat->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$f_riwayat->move('asset/file/dokumen/drh', $new_name); /* MEMINDAH FILE */
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_riwayat" => $new_name
					]);
		}

		if(Input::hasFile('filevisiMisi')){
			$ext = File::extension($f_visi->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			$f_visi->move('asset/file/dokumen/visi_misi', $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_visi" => $new_name
					]);
		}

		if(Input::hasFile('filefotoCopyKtp')){
			$ext = File::extension($f_ktp->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filefotoCopyKtp')->move(asset('asset/file/dokumen/ktp'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_ktp" => $new_name
					]);
		}

		if(Input::hasFile('filefotoCopyKk')){
			$ext = File::extension($f_kk->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filefotoCopyKk')->move(asset('asset/file/dokumen/kk'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_kk" => $new_name
					]);
		}

		if(Input::hasFile('filefotoCopyNpwp')){
			$ext = File::extension($f_npwp->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filefotoCopyNpwp')->move(asset('asset/file/dokumen/npwp'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_npwp" => $new_name
					]);
		}

		if(Input::hasFile('filefotoCopyIjazah')){
			$ext = File::extension($f_ijazah->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filefotoCopyIjazah')->move(asset('asset/file/dokumen/ijazah'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_ijazah" => $new_name
					]);
		}

		if(Input::hasFile('fileskcs')){
			$ext = File::extension($f_skck->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('fileskcs')->move(asset('asset/file/dokumen/skck'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_skck" => $new_name
					]);
		}

		if(Input::hasFile('filektaPartaiHanura')){
			$ext = File::extension($f_kta->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filektaPartaiHanura')->move(asset('asset/file/dokumen/kta'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_kta" => $new_name
					]);
		}

		if(Input::hasFile('fileformPendaftaranCalon')){
			$ext = File::extension($f_daftar->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('fileformPendaftaranCalon')->move(asset('asset/file/dokumen/form_pendaftaran'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_pendaftaran" => $new_name
					]);
		}

		if(Input::hasFile('filekomitmen')){
			$ext = File::extension($f_komit->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filekomitmen')->move(asset('asset/file/dokumen/komitmen'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_komitmen" => $new_name
					]);
		}

		if(Input::hasFile('filetinggal')){
			$ext = File::extension($f_nkri->getClientOriginalName());
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext;
			Input::file('filetinggal')->move(asset('asset/file/dokumen/nkri'), $new_name);
			
			DB::table('caleg_drh')
				->where('id',$key)
				->update([
					"file_nkri" => $new_name
					]);
		}
		return redirect(asset('checklist'));
	}
}
?>