<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;

class SurveiController extends Controller{
	public function index(){
		$getData = DB::table('caleg_drh')
				->join('t_survei',
							't_survei.id_calon',
							'=',
							'caleg_drh.id')
				->join('m_geo_prov_kpu',
						'caleg_drh.tingkat_provinsi',
						'=',
						'm_geo_prov_kpu.geo_prov_id')
				->leftJoin('m_geo_kab_kpu',
						'caleg_drh.tingkat_kabupaten',
						'=',
						'm_geo_kab_kpu.geo_kab_id')
				->orderBy('type')->get();
		$getProv = DB::table('m_geo_prov_kpu')->get();
		return view('main.survei.index', array(
			"dataProv" 	=> $getProv,
			"dataBalon" => $getData,
			"menu" => "hasil-survei"
			));
	}

	public function view_page_input(){
		$dataProv = DB::table('m_geo_prov_kpu')->get();
		return view('main.survei.input_survei', array(
			"dataProv" => $dataProv,
			"menu" 	   => "hasil-survei"
			));
	}
	
	public function action_survei($key){
		if($key == "input"){
			$jumlah = Input::get('jumlahData');
			$responder = Input::get('jmlResponden');

			$a = 1;
			while($a <= $jumlah){
				$idc = Input::get('id_calon'.$a);
				$sur = Input::get('hasil_survei'.$a);

				DB::table('t_survei')
					->insertGetId([
						'id_calon' => $idc,
						'survei'   => $sur,
						'responder'=> $responder
						]);
				$a++;
			}
		}else if($key == "edit"){
			$id_calon = Input::get('id_calon');
			$n_survei = Input::get('n_survei');
			$responder = Input::get('n_responder');

			$update = DB::table('t_survei')
						->where('id_calon', $id_calon)
						->update([
							'survei' => $n_survei,
							'responder' => $responder
							]);
		}else{

		}
		return redirect('hasil-survei');
	}

	public function edit_survei(){
		return redirect('hasil-survei');
	}
}
?>