<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;

date_default_timezone_set('Asia/Jakarta');
class AgendaController extends Controller{
	public function index(){
		$date = date('d M Y');
		$dss  = date('Y-m-d');
		$getAgenda = DB::table('agenda')->orderBy('waktu_mulai', 'asc')->get();
		return view('main.agenda.index', array(
			"dataAgenda" => $getAgenda,
			"date" => $date,
			"dss"  => $dss,
			"menu" => "agenda"
			));
	}

	public function add_action(){
		$kegiatan  = Input::get('inp_agenda');
		$pelaksana = Input::get('inp_pelaksana');
		$tempat	   = Input::get('inp_tempat');
		$desk	   = Input::get('inp_deskripsi');

		if(Input::get('inp_date_start') != ""){
			$w_mulai   = Input::get('inp_date_start');
		}else{
			$w_mulai = null;
		}

		if(Input::get('inp_date_end') != ""){
			$w_end	   = Input::get('inp_date_end');
		}else{
			$w_end     = null;
		}

		$insert = DB::table('agenda')
					->insertGetId([
						'kegiatan'  => $kegiatan,
						'pelaksana' => $pelaksana,
						'tempat'    => $tempat,
						'waktu_mulai'   => $w_mulai,
						'waktu_selesai' => $w_end,
						'deskripsi' => $desk
						]);
		return redirect('agenda');
	}

	public function view_add(){
		return view('main.agenda.page-add', array(
			"menu" => "agenda"
			));
	}
}
?>