<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use Redirect;
use DB;
use HelperData;

class DashboardController extends Controller{
	public function index(){
		if(Session::get('username')){
			return redirect('dashboard');
		}else{
			return view('main.login');
		}
	}

	public function proses_login(){
		$arr = array();
		$user = Input::get('user');
		$pass = Input::get('pass');
		/*echo $user." ".$pass;*/
		$num = DB::table('m_users')
				->where('username', $user)
				->where('password', $pass)
				->count();
		if($num == 1){
			$data = DB::table('m_users')
				->where('username', $user)
				->where('password', $pass)
				->get();
			foreach ($data as $get) {
				session()->put('role', $get->role);
				session()->put('kode', $get->kode_user);
				session()->put('username', $user);
				session()->put('cl_admin',$get->id);
				session()->put('idLogin',$get->id);
			}
			$arr = array("status" => "Success", "role" => session('role'));
		}else{
			$arr = array("status" => "failed", "message" => "Maaf, username atau password salah.");
		}
		echo json_encode($arr);
	}

	public function view_dashboard(){
		$dataProv = DB::table('m_geo_prov_kpu')->get();
		
		$dataProvP = DB::table('m_geo_prov_kpu')
						->where('geo_prov_id', "25823")
						->get();

		$dataKotaP  = DB::table('m_geo_prov_kpu')->get();
		$dataKabP  = DB::table('m_geo_kab_kpu')->select('m_geo_kab.geo_kab_lat','m_geo_kab.geo_kab_lng', 'm_geo_kab_kpu.geo_kab_nama')->join('m_geo_kab', 'm_geo_kab_kpu.geo_kab_id2', '=', 'm_geo_kab.geo_kab_id')->get();

		$getSK    = DB::table('caleg_drh')->where('no_sk','!=','')->count();
		$getBalon = DB::table('caleg_drh')->where('no_sk','=','')->count();
		$jumlah   = $getSK+$getBalon;

		$getProvinsi = DB::select("SELECT * FROM caleg_drh
									WHERE 
									type='Gubernur'
									OR type='Wakil Gubernur'");
		$getKota = DB::select("SELECT * FROM caleg_drh
									WHERE 
									type='Walikota'
									OR type='Wakil Walikota'");
		$getKab = DB::select("SELECT * FROM caleg_drh
									WHERE 
									type='Bupati'
									OR type='Wakil Bupati'");

		return view('main.dashboard.index', array(
			'menu'    	   => "dashboard",
			'dataProv'     => $dataProv,
			'jumlah_sk'    => $getSK,
			'jumlah_balon' => $getBalon,
			'jumlah_semua' => $jumlah,
			'dataProvP'    => $dataProvP,
			'dataKotaP'	   => $dataKotaP,
			'dataKabP'     => $dataKabP,
			'jumlah_prov'  => count($dataProvP),
			'jumlah_kota'  => count($dataKotaP),
			'jumlah_kab'  => count($dataKabP),
			));
	}

	public function logout(){
		Session::flush();
		return redirect('/');
	}
}
?>