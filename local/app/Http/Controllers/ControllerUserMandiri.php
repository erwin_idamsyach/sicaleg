<?php namespace App\Http\Controllers;

use Session;
use Input;
use Redirect;
use DB;
use HelperData;

class ControllerUserMandiri extends Controller{
	public function index(){
		$kode = session('kode');
		return view('main.user.index', array(
			"menu" => "dashboard"
			));
	}

	public function edit_bio(){
		$kode = session('kode');
		$dataProv = DB::table('m_geo_prov_kpu')->get();
		$dataUser = DB::table('caleg_drh')
						->where('id', $kode)
						->get();
		return view('main.user.edit.bio', array(
			"menu" => "biodata",
			"data" => $dataUser,
			"dataProvinsi" => $dataProv
			));
	}
}
?>