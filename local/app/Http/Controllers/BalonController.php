<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use Redirect;
use DB;

class BalonController extends Controller{
	public function index_balon(){
		$getProv = DB::table('m_geo_prov_kpu')->get();
		$getData = DB::table('caleg_drh')
					->join('caleg_dapil',
							'caleg_drh.id',
							'=',
							'caleg_dapil.caleg_id')
					->join('m_geo_kab_kpu',
							'caleg_drh.tempat_lahir',
							'=',
							'm_geo_kab_kpu.geo_kab_id')
					->join('tingkat_pemilihan',
						'tingkat_pemilihan.id',
						'=',
						'caleg_dapil.tingkat_caleg')
					->select(
						'caleg_drh.type',
						'caleg_drh.id',
						'caleg_drh.nama',
						'caleg_drh.tempat_lahir',
						'caleg_drh.tanggal_lahir',
						'caleg_dapil.dapil',
						'm_geo_kab_kpu.geo_kab_nama',
						'tingkat_pemilihan.tingkat_caleg'
						)
					->where('tahun', DB::Raw("YEAR(NOW())"))
					->where('flag', 'Balon')
					->get();
		/*$getData = DB::table('caleg_drh')
						->join('m_geo_prov_kpu',
								'caleg_drh.tingkat_provinsi',
								'=',
								'm_geo_prov_kpu.geo_prov_id')
						->join('m_geo_kab_kpu',
								'caleg_drh.tingkat_kabupaten',
								'=',
								'm_geo_kab_kpu.geo_kab_id')
						->select('caleg_drh.id',
								'caleg_drh.tempat_lahir',
								'caleg_drh.tanggal_lahir',
								'caleg_drh.type',
								'caleg_drh.nama',
								'm_geo_prov_kpu.geo_prov_nama',
								'm_geo_kab_kpu.geo_kab_nama')
						->where('flag','Balon')
						->where('tahun', DB::raw('YEAR(NOW())'))
						->get();*/
		return view('main.balon.index', array(
			'dataBalonKokab' => $getData,
			'dataProv'       => $getProv,
			'menu' 			 => "balon_list"
			));
	}

	public function open_input_form(){
		$getProv = DB::table('m_geo_prov_kpu')->get();
		return view('main.balon.kokab.add-index', array(
			'dataProvinsi' => $getProv,
			'menu'     => 'balon-kokab'
			));
	}

	public function index_balon_provinsi(){
		$getProv = DB::table('m_geo_prov_kpu')->get();
		$getData = DB::select("SELECT
							caleg_drh.id,
							caleg_drh.tempat_lahir,
							caleg_drh.tanggal_lahir,
							caleg_drh.type,
							caleg_drh.nama,
							m_geo_prov_kpu.geo_prov_nama							
							FROM 
							caleg_drh
							LEFT JOIN 
							m_geo_prov_kpu
							ON
							caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
							WHERE
							type = 'Gubernur' OR type='Wakil Gubernur'
							AND flag='Balon'");
		/*$getData = DB::table('caleg_drh')
						->join('m_geo_prov_kpu',
								'caleg_drh.tingkat_provinsi',
								'=',
								'm_geo_prov_kpu.geo_prov_id')
						->select('caleg_drh.id',
								'caleg_drh.type',
								'caleg_drh.nama',
								'caleg_drh.flag',
								'm_geo_prov_kpu.geo_prov_nama'
								)
						->where('type', '=', 'Gubernur')
						->where('type', '=', 'Wakil Gubernur')
						->get();*/
		return view('main.balon.provinsi.index', array(
			'dataBalonProvinsi' => $getData,
			'dataProv'       => $getProv,
			'menu' 			 => "balon-provinsi"
			));
	}

	public function open_input_form_provinsi(){
		$getProv = DB::table('m_geo_prov_kpu')->get();
		return view('main.balon.add-index', array(
			'dataProvinsi' => $getProv,
			'menu'     => 'balon-input'
			));
	}

	public function action_prov(){
		$jenkada = Input::get('jenis_kada');
		$prov_p  = Input::get('dapil_provinsi');
		$jenis_id= Input::get('identitas');
		$no_id   = Input::get('noIdentitas');
		$nama    = Input::get('namaDepan')." ".Input::get('namaTengah')." ".Input::get('namaBelakang');
		$tempat_lahir = Input::get('tempatLahir');
		$tgl_lahir = Input::get('tanggalLahir');
		$agama   = Input::get('agama');
		$jenkel  = Input::get('jenisKelamin');
		$alamat  = Input::get('alamat');
		$prov_a  = Input::get('abProv');
		$kab_a  = Input::get('abKab');
		$kec_a  = Input::get('abKec');
		$kel_a  = Input::get('abKel');
		$status_nikah  = Input::get('statusPernikahan');
		$nama_pasangan = Input::get('namaPasangan');
		$jumlah_anak   = Input::get('jumlahAnak');
		$telpon = Input::get('telp');
		$hp     = Input::get('hp');
		$email  = Input::get('emailBalon');
		$twitter = Input::get('twitter');
		$facebook = Input::get('facebook');

		$id_insert = DB::table('caleg_drh')
						->insertGetId([
							'type' => $jenkada,
							'tingkat_provinsi' => $prov_p,
							'nama' => $nama,
							'jenis_identitas'  => $jenis_id,
							'nomer_identitas'  => $no_id,
							'tempat_lahir'  => $tempat_lahir,
							'tanggal_lahir' => $tgl_lahir,
							'jenis_kelamin' => $jenkel,
							'agama' => $agama,
							'alamat'=> $alamat,
							'provinsi' => $prov_a,
							'kabupaten'=> $kab_a,
							'kecamatan'=> $kec_a,
							'kelurahan'=> $kel_a,
							'telephone'=> $telpon,
							'handphone'=> $hp,
							'email' => $email,
							'facebook' => $facebook,
							'twitter'  => $twitter,
							'status_kawin' => $status_nikah,
							'pasangan' => $nama_pasangan,
							'anak'  => $jumlah_anak,
							'create_date' => date('Y-m-d H:i:s')
							]);
		
		$jml_pendidikan = Input::get('jml_pendidikan');
		$a = 1;
		while($a <= $jml_pendidikan){
			$pend = Input::get('pendidikan_tahun'.$a);
			$ket = Input::get('pendidikan_keterangan'.$a);
			DB::table('caleg_pendidikan')
					->insert([
						'caleg_drh_id' => $id_insert,
						'tahun' => $pend,
						'keterangan' => $ket
						]);
			$a++;
		}

		$jml_org = Input::get('jml_organisasi');
		$b = 1;
		while($b <= $jml_org){
			$pend = Input::get('organisasi_tahun'.$b);
			$ket = Input::get('organisasi_keterangan'.$b);
			DB::table('caleg_organisasi')
					->insert([
						'caleg_drh_id' => $id_insert,
						'tahun' => $pend,
						'keterangan' => $ket
						]);
			$b++;
		}

		$jml_pekerjaan = Input::get('jml_pekerjaan');
		$c = 1;
		while($c <= $jml_pekerjaan){
			$pend = Input::get('pekerjaan_tahun'.$c);
			$ket = Input::get('pekerjaan_keterangan'.$c);
			DB::table('caleg_pekerjaan')
					->insert([
						'caleg_drh_id' => $id_insert,
						'tahun' => $pend,
						'keterangan' => $ket
						]);
			$c++;
		}

		$jml_diklat = Input::get('jml_diklat');
		$d = 1;
		while($d <= $jml_diklat){
			$pend = Input::get('diklat_tahun'.$d);
			$ket = Input::get('diklat_keterangan'.$d);
			DB::table('caleg_diklat')
					->insert([
						'caleg_drh_id' => $id_insert,
						'tahun' => $pend,
						'keterangan' => $ket
						]);
			$d++;
		}

		$jml_perjuangan = Input::get('jml_perjuangan');
		$e = 1;
		while($e <= $jml_perjuangan){
			$pend = Input::get('perjuangan_tahun'.$e);
			$ket = Input::get('perjuangan_keterangan'.$e);
			DB::table('caleg_perjuangan')
					->insert([
						'caleg_drh_id' => $id_insert,
						'tahun' => $pend,
						'keterangan' => $ket
						]);
			$e++;
		}

		$jml_penghargaan = Input::get('jml_penghargaan');
		$f = 1;
		while($f <= $jml_penghargaan){
			$pend = Input::get('penghargaan_tahun'.$f);
			$ket = Input::get('penghargaan_keterangan'.$f);
			DB::table('caleg_penghargaan')
					->insert([
						'caleg_drh_id' => $id_insert,
						'tahun' => $pend,
						'keterangan' => $ket
						]);
			$f++;
		}
		return redirect('bakal-calon/provinsi');
	}

	public function send_data_prov(){
		$key = Input::get('key');
		$arr = array();
		$getData = DB::select("SELECT
							caleg_drh.*,
							m_geo_prov_kpu.geo_prov_nama,
							m_geo_kec_kpu.geo_kec_nama,
							m_geo_kab_kpu.geo_kab_nama
							FROM 
							caleg_drh
							LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id 
								AND caleg_drh.provinsi = m_geo_prov_kpu.geo_prov_id
							LEFT JOIN m_geo_kab_kpu ON caleg_drh.kabupaten = m_geo_kab_kpu.geo_kab_id
							LEFT JOIN m_geo_kec_kpu ON caleg_drh.kecamatan = m_geo_kec_kpu.geo_kec_id
							WHERE
							id='$key'");
		foreach ($getData as $get) {
			$arr = array(
				"kategori" => $get->type,
				"provinsi" => $get->geo_prov_nama,
				"nama"     => $get->nama,
				"jenis_id" => $get->jenis_identitas,
				"no_id"    => $get->nomer_identitas,
				"tempat_lahir" => $get->tempat_lahir,
				"tgl_lahir"=> $get->tanggal_lahir,
				"jenkel"   => $get->jenis_kelamin,
				"status_nikah" => $get->status_kawin,
				"nama_pasangan"=> $get->pasangan,
				"alamat"   => $get->alamat,
				"kecamatan"=> $get->geo_kec_nama,
				"kabupaten"=> $get->geo_kab_nama,
				"provinsis" => $get->geo_prov_nama,
				"agama"    => $get->agama,
 				);
		}
		echo json_encode($arr);
	}

	public function getDataEdit($key){
		$dataProv = DB::table('m_geo_prov_kpu')->get();
		$getData = DB::table('caleg_drh')
						->where('id', $key)
						->get();
		return view('main.edit.balon.edit', array(
			"dataEdit" => $getData,
			"dataProvinsi" => $dataProv,
			"menu" => "balon-provinsi"	
			));
	}
	public function getDataEditKokab($key){
		$dataProv = DB::table('m_geo_prov_kpu')->get();
		$getData = DB::table('caleg_drh')
						->where('id', $key)
						->get();
		return view('main.edit.balon.kokab.edit', array(
			"dataEdit" => $getData,
			"dataProvinsi" => $dataProv,
			"menu" => "balon-kokab"	
			));
	}
}
?>