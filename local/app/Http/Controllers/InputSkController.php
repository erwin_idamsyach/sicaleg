<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;

class InputSkController extends Controller{
	public function index_kokab(){
		$getKokab = DB::select("
						SELECT
							caleg_drh.*, t_survei.*
						FROM
						caleg_drh 
						JOIN t_survei ON t_survei.id_calon = caleg_drh.id
						WHERE
						type ='Walikota'
						OR type='Bupati'
						OR type='Wakil Walikota'
						OR type='Wakil Bupati'
						ORDER BY flag ASC
						");
		return view('main.sk.kokab.index', array(
			'dataKokab' => $getKokab,
			'menu'      => 'sk-kokab'
			));
	}

	public function edit_kokab($key){
		$getPartai = DB::table('partai')->where('FLAG','=','RELEASED')->get();
		$getBalon = DB::select("
						SELECT * FROM
						caleg_drh
						LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
						LEFT JOIN m_geo_kab_kpu ON caleg_drh.tingkat_kabupaten = m_geo_kab_kpu.geo_kab_id
						WHERE
						id='$key'
						");
		return view('main.sk.kokab.form-edit', array(
			'dataBalon' => $getBalon,
			'dataPartai'=> $getPartai,
			'menu'      => 'sk-kokab'
			));
	}

	public function kokab_action(){
		$id 	 = Input::get('balon_id');
		$no_sk 	 = Input::get('no_sk');
		$perihal = Input::get('perihal');
		$periode_1 = Input::get('periode_1');
		$periode_2 = Input::get('periode_2');
		$sebagai = Input::get('sebagai');
		$tgl_sk  = Input::get('tgl_sk');
		$no_tpp  = Input::get('no_tpp');;
		$kategori= Input::get('kategori');
		$kader   = Input::get('kader');
		$kursi_1 = Input::get('jumlah_kursi');
		$kursi_2 = Input::get('kursi_hanura');
		$tgl_1	 = Input::get('tgl_rapat');
		$tgl_2	 = Input::get('tgl_putusan');
		$survei  = Input::get('hasil_survei');
		$file_sk = Input::file('file_sk');
		$file_sur= Input::file('file_survei');

		$destinationPath = "asset/file/sk/";
		$extension    = File::extension($file_sk->getClientOriginalName());

		$destinationPathS = "asset/file/survei/";
		$extensionS    = File::extension($file_sur->getClientOriginalName());
		$file_name_sk = rand(111111, 999999)."_".date('His').$extension;
		$file_sk->move("asset/file/sk", $file_sk->getClientOriginalName());
		$file_name_sur = rand(111111, 999999)."_".date('His').$extensionS;
		$file_sur->move("asset/file/survei", $file_sur->getClientOriginalName());

		$input = DB::table('surat_keputusan')
					->insert([
						'id_caleg_drh' => $id,
						'no_sk' => $no_sk,
						'perihal' => $perihal,
						'periode' => $periode_1,
						'periode2'=> $periode_2,
						'sebagai' => $sebagai,
						'tanggal_sk' => $tgl_sk,
						'no_tpp'  => $no_tpp,
						'kategori_daerah' => $kategori,
						'status_kader' => $kader,
						'persyaratan'  => $kursi_1,
						'kursi_partai' => $kursi_2,
						'tanggal_catatan1' => $tgl_1,
						'tanggal_catatan2' => $tgl_2,
						'hasil_survei' => $survei,
						'foto_sk' => $file_sk->getClientOriginalName(),
						'foto_hasil_survei' => $file_sur->getClientOriginalName()
						]);
		$update = DB::table('caleg_drh')
					->where('id', $id)
					->update([
						'flag' => 'Calon',
						'no_sk'=> $no_sk,
						'foto_sk' => $file_name_sk,
						'status_kader' => $kader
						]);
		return redirect(asset('sk-kokab'));
	}

	public function index_provinsi(){
		$getProv = DB::select("
						SELECT caleg_drh.*, t_survei.* FROM
						caleg_drh 
						JOIN t_survei ON t_survei.id_calon = caleg_drh.id
						WHERE
						type = 'Gubernur'
						OR type = 'Wakil Gubernur'
						ORDER BY flag, type ASC
						");
		return view('main.sk.provinsi.index', array(
			'dataProv' => $getProv,
			'menu'      => 'sk-provinsi'
			));
	}

	public function edit_provinsi($key){
		$getPartai = DB::table('partai')->where('FLAG','=','RELEASED')->get();
		$getBalon = DB::select("
						SELECT * FROM
						caleg_drh
						LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
						LEFT JOIN m_geo_kab_kpu ON caleg_drh.tingkat_kabupaten = m_geo_kab_kpu.geo_kab_id
						WHERE
						id='$key'
						");
		return view('main.sk.provinsi.form-edit', array(
			'dataBalon' => $getBalon,
			'dataPartai'=> $getPartai,
			'menu'      => 'sk-provinsi'
			));
	}
}
?>