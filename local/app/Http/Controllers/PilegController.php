<?php namespace App\Http\Controllers;
/* 
PILEG CONTROLLER
Create By : Erwin Idamsyach Putra
Location  : PT. ICSP Jakarta, Indonesia
*/
use DB;
use Input;
use File;
use Session;
use Request;
class PilegController extends Controller{
	public function index(){
		$getTingkat  = DB::table('tingkat_pemilihan')->get();
		$getProvinsi = DB::table('m_geo_prov_kpu')->get();
		$getDapilRi  = DB::table('dapil_dpr_ri')->get();
		$getDapilTSatu = DB::table('dapil_dprd_1')->get();
		$getDapilTDua  = DB::table('dapil_dprd_2')->get();
		return view('main.bacaleg.index', array(
			'dataTingkat'   => $getTingkat,
			'dataProv'      => $getProvinsi,
			'dataDapilRi'   => $getDapilRi,
			'dataDapilSatu' => $getDapilTSatu,
			'dataDapilDua'  => $getDapilTDua
		));
	}

	public function input_data_calon(Request $request){
		$jenis_identitas = Input::get('identitas');
		$no_identitas 	= Input::get('noIdentitas');

		$cekData = DB::table('caleg_drh')
					->where('jenis_identitas', $jenis_identitas)
					->where('nomer_identitas', $no_identitas)
					->count();

		if($cekData == 1){
			Session::flash('error', 'Data sudah ada!');
			return redirect('pileg2019');
		}else{
			$kategori = Input::get('kategori_caleg');

			if($kategori == 1){
				$dapil = Input::get('dapil');
				$dapil2 = Input::get('dapil2');
			}else if($kategori == 2){
				$dapil = Input::get('dapil_prov');
				$dapil2 = Input::get('dapil_prov_2');
			}else{
				$dapil = Input::get('dapil_kota');
				$dapil2 = Input::get('dapil_kota7');
			}

			$nama_depan 	= Input::get('namaDepan');
			$nama_tengah	= Input::get('namaTengah');
			$nama_belakang  = Input::get('namaBelakang');
			$nama 			= $nama_depan." ".$nama_tengah." ".$nama_belakang;
			$gelar_depan    = Input::get('gelar_depan');
			$gelar_belakang = Input::get('gelar_belakang');
			$tempat_lahir   = Input::get('tempatLahir');
			$tgl_lahir 		= Input::get('tanggalLahir');
			$agama			= Input::get('agama');
			$jenkel			= Input::get('jenisKelamin');
			$alamat			= Input::get('alamat');
			$a_prov			= Input::get('abProv');
			$a_kota			= Input::get('abKab');
			$a_kec			= Input::get('abKec');
			$a_kel			= Input::get('abKel');
			$status_nikah 	= Input::get('statusPernikahan');
			$jabatan_hanura = Input::get('jabatan_hanura');
			$jabatan_diluar = Input::get('jabatan_diluar_hanura');
			$telpon    		= Input::get('telpon');
			$handphone 		= Input::get('handphone');
			$email 	   		= Input::get('email');
			$twitter   		= Input::get('twitter');
			$facebook  		= Input::get('facebook');
			$foto			= Input::file('foto');

			if($status_nikah == "menikah"){
				$nama_pasangan = Input::get('namaPasangan');
				$jumlah_anak   = Input::get('jumlahAnak');
			}else{
				$nama_pasangan = '';
				$jumlah_anak   = '';
			}

			$insert = DB::table('caleg_drh')
					->insertGetId([
						'type' => $kategori,
						'nama' => $nama,
						'jenis_identitas' => $jenis_identitas,
						'nomer_identitas' => $no_identitas,
						'tempat_lahir'	  => $tempat_lahir,
						'tanggal_lahir'	  => $tgl_lahir,
						'jenis_kelamin'	  => $jenkel,
						'agama' 		  => $agama,
						'alamat'		  => $alamat,
						'provinsi'		  => $a_prov,
						'kabupaten'		  => $a_kota,
						'kecamatan'		  => $a_kec,
						'kelurahan'		  => $a_kel,
						'status_kawin'	  => $status_nikah,
						'pasangan'		  => $nama_pasangan,
						'anak'			  => $jumlah_anak,
						'jabatan_hanura'  => $jabatan_hanura,
						'jabatan_diluar_hanura' => $jabatan_diluar,
						'telephone'		  => $telpon,
						'handphone'		  => $handphone,
						'email'			  => $email,
						'twitter'		  => $twitter,
						'facebook'		  => $facebook,
						'flag'			  => "Balon",
						'tahun'			  => date('Y')
					]);

			DB::table('caleg_dapil')
					->insert([
						"caleg_id" 		=> $insert,
						"tingkat_caleg" => $kategori,
						"dapil"         => $dapil
					]);
	/*------------------------------------------------- I N S E R T   P H A S E   1--------------------------------*/
			$jml_pendidikan = Input::get('jml_pendidikan');
			$a = 1;
			while($a <= $jml_pendidikan){
				$pend = Input::get('pendidikan_tahun'.$a);
				$ket = Input::get('pendidikan_keterangan'.$a);
				DB::table('caleg_pendidikan')
						->insert([
							'caleg_drh_id' => $insert,
							'tahun' => $pend,
							'keterangan' => $ket
							]);
				$a++;
			}

			$jml_org = Input::get('jml_organisasi');
			$b = 1;
			while($b <= $jml_org){
				$pend = Input::get('organisasi_tahun'.$b);
				$ket = Input::get('organisasi_keterangan'.$b);
				DB::table('caleg_organisasi')
						->insert([
							'caleg_drh_id' => $insert,
							'tahun' => $pend,
							'keterangan' => $ket
							]);
				$b++;
			}

			$jml_pekerjaan = Input::get('jml_pekerjaan');
			$c = 1;
			while($c <= $jml_pekerjaan){
				$pend = Input::get('pekerjaan_tahun'.$c);
				$ket = Input::get('pekerjaan_keterangan'.$c);
				DB::table('caleg_pekerjaan')
						->insert([
							'caleg_drh_id' => $insert,
							'tahun' => $pend,
							'keterangan' => $ket
							]);
				$c++;
			}

			$jml_diklat = Input::get('jml_diklat');
			$d = 1;
			while($d <= $jml_diklat){
				$pend = Input::get('diklat_tahun'.$d);
				$ket = Input::get('diklat_keterangan'.$d);
				DB::table('caleg_diklat')
						->insert([
							'caleg_drh_id' => $insert,
							'tahun' => $pend,
							'keterangan' => $ket
							]);
				$d++;
			}

			$jml_perjuangan = Input::get('jml_perjuangan');
			$e = 1;
			while($e <= $jml_perjuangan){
				$pend = Input::get('perjuangan_tahun'.$e);
				$ket = Input::get('perjuangan_keterangan'.$e);
				DB::table('caleg_perjuangan')
						->insert([
							'caleg_drh_id' => $insert,
							'tahun' => $pend,
							'keterangan' => $ket
							]);
				$e++;
			}

			$jml_penghargaan = Input::get('jml_penghargaan');
			$f = 1;
			while($f <= $jml_penghargaan){
				$pend = Input::get('penghargaan_tahun'.$f);
				$ket = Input::get('penghargaan_keterangan'.$f);
				DB::table('caleg_penghargaan')
						->insert([
							'caleg_drh_id' => $insert,
							'tahun' => $pend,
							'keterangan' => $ket
							]);
				$f++;
			}
	/*------------------------------------------------- I N S E R T   P H A S E   2--------------------------------*/
			if(Input::hasFile('foto')){
				/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
				$ext = File::extension($foto->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
				$new_name = strtotime(date('Y-m-d H:i:s'))."_".$insert.".".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
				$foto->move('asset/file/foto_caleg', $new_name); /* MEMINDAH FILE */
				
				DB::table('caleg_drh')
					->where('id',$insert)
					->update([
						"foto" => $new_name
						]);
			}

			return redirect('selamat-datang/'.$insert);

		}

	}

	public function selamat_datang($id){
			return view('main.bacaleg.selamat-datang');
		}
}

?>