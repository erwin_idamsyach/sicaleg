<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;

class SeleksiController extends Controller{
	public function index_kokab(){
		$getDataCalon = DB::table('caleg_drh')
						->join('t_survei',
								'caleg_drh.id',
								'=',
								't_survei.id_calon')
						->where('caleg_drh.type', "Walikota")
						->orWhere('caleg_drh.type', "Wakil Walikota")
						->orWhere('caleg_drh.type', "Bupati")
						->orWhere('caleg_drh.type', "Wakil BUpati")
						->get();
		return view('main.seleksi.kokab.index', array(
					'menu' => "seleksi-kokab",
					'dataCalon' => $getDataCalon
			));
	}
}
?>