<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use Redirect;
use DB;

class MasterController extends Controller{
	/* CONTROLLER FOR MASTER LEVEL USER */
	public function view_level(){
		$dataLevel = DB::table('level_user')->get();
		return view('main.master.level.index', array(
			'dataLevel' => $dataLevel,
			'menu'		=> "menu-level"
			));
	}

	public function load_level(){
		$dataLevel = DB::table('level_user')->get();
		return view('main.master.level.load', array(
			'dataLevel' => $dataLevel
			));
	}

	public function get_level(){
		$arr = array();
		$id = Input::get('id');
		$getLevel = DB::table('level_user')
					->where('id', $id)
					->get();
		foreach ($getLevel as $get) {
			$arr = array(
				'level' => $get->level,
				'id' => $get->id
				);
		}
		echo json_encode($arr);
	}

	public function add_level(){
		$isi = Input::get('isi');
		$add = DB::table('level_user')
				->insert([
					'level' => $isi,
					'create_by' => "ADMIN",
					'create_date' => date('Y-m-d H:i:s'),
					'flag'  => "USED"
					]);
	}

	public function edit_level(){
		$isi = Input::get('isi');
		$id  = Input::get('id');

		$edit = DB::table('level_user')
				->where('id', $id)
				->update(['level' => $isi]);
	}

	public function delete_level(){
		$id  = Input::get('id');
		$delete = DB::table('level_user')
					->where('id', $id)
					->delete();
	}

	/* CONTROLLER FOR MASTER USER */
	public function view_user(){
		$getMasterLevel = DB::table('level_user')
							->where('level', '!=', 'UserAccessKetua')
							->get();
		$getData = DB::table('user')
					->join('level_user', 'user.level', '=', 'level_user.id')
					->select('user.username', 'user.email','user.id','level_user.level')
					->where('user.flag','=','USED')
					->get();
		return view('main.master.user.index', array(
			'dataUser' => $getData,
			'dataLevel'=> $getMasterLevel,
			'menu'     => "menu-user"
			));
	}

	public function load_user(){
		$getData = DB::table('user')
					->join('level_user', 'user.level', '=', 'level_user.id')
					->select('user.username', 'user.email','user.id','level_user.level')
					->where('user.flag','=','USED')
					->get();
		return view('main.master.user.load', array(
			'dataUser' => $getData
			));
	}

	public function add_user(){
		$username = Input::get('username');
		$password = Input::get('password');
		$email	  = Input::get('email');
		$level    = Input::get('level');

		$add = DB::table('user')
				->insert([
					'username' => $username,
					'password' => md5($password),
					'email'    => $email,
					'level'    => $level,
					'flag'     => "USED",
					'create_by'=> "admin",
					'create_date' => date('Y-m-d H:i:s')
					]);
	}
}
?>