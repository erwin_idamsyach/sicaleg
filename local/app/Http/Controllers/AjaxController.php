<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;

class AjaxController extends Controller{
	public function data_balon(){
		$key = Input::get('key');
		$arr = array();

		$getData = DB::select("SELECT * 
						FROM
						caleg_drh
						LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
						LEFT JOIN m_geo_kab_kpu ON caleg_drh.tingkat_kabupaten = m_geo_kab_kpu.geo_kab_id
						LEFT JOIN surat_keputusan ON caleg_drh.id = surat_keputusan.id_caleg_drh
						WHERE
						caleg_drh.id='$key'");
		foreach ($getData as $get) {
			$tgl_1 = date_create($get->tanggal_catatan1);
			$tgl_2 = date_create($get->tanggal_catatan2);
			$tgl_3 = date_create($get->tanggal_sk);
			$tgl_lahir = date_create($get->tanggal_lahir);
			$arr = array(
				"nama" => $get->nama,
				"no_id" => $get->nomer_identitas,
				"jenis_id" => $get->jenis_identitas,
				"provinsi" => $get->geo_prov_nama,
				"kota"     => $get->geo_kab_nama,
				"tempat_lahir" => $get->tempat_lahir,
				"tgl_lahir"    => date_format($tgl_lahir,"d-M-Y"),
				"jenkel"   => $get->jenis_kelamin,

				"no_sk"    => $get->no_sk,
				"perihal"  => $get->perihal,
				"sebagai"  => $get->sebagai,
				"no_tpp"   => $get->no_tpp,
				"kategori" => $get->kategori_daerah,
				"syarat_kursi_1" => $get->persyaratan,
				"syarat_kursi_2" => $get->kursi_partai,
				"tgl_sk"   => date_format($tgl_3, "d-M-Y"),
				"periode"  => $get->periode." - ".$get->periode2,
				"kader"    => $get->status_kader,
				"tgl_catat_1"    => date_format($tgl_1, "d-M-Y"),
				"tgl_catat_2"    => date_format($tgl_2, "d-M-Y"),
				"hasil_survei"   => $get->hasil_survei
 				);
		}
		echo json_encode($arr);
	}

	public function cek_identitas(){
		$jenis = Input::get('jenis');
		$nomer = Input::get('nomer');

		$cek = DB::table('caleg_drh')
					->where('jenis_identitas',$jenis)
					->where('nomer_identitas',$nomer)
					->count();
		if($cek == 1){
			echo "FAILED";
		}else{
			echo "success";
		}
	}

	public function getKab(){
		echo "<option value='' selected disabled>--Pilih Kota/Kabupaten--</option>";
		$key = Input::get('key');
		$getData = DB::table('m_geo_kab_kpu')
						->where('geo_prov_id', $key)
						->get();
		foreach($getData as $get){
			echo "<option value='".$get->geo_kab_id."'>".$get->geo_kab_nama."</option>";
		}
	}

	public function getKecamatan(){
		echo "<option value='' selected disabled>--Pilih Kecamatan--</option>";
		$key = Input::get('key');
		$getData = DB::table('m_geo_kec_kpu')
						->where('geo_kab_id', $key)
						->get();
		foreach($getData as $get){
			echo "<option value='".$get->geo_kec_id."'>".$get->geo_kec_nama."</option>";
		}
	}

	public function getKelurahan(){
		echo "<option value='' selected disabled>--Pilih Kelurahan--</option>";
		$key = Input::get('key');
		$getData = DB::table('m_geo_deskel_kpu')
						->where('geo_kec_id', $key)
						->get();
		foreach($getData as $get){
			echo "<option value='".$get->geo_deskel_id."'>".$get->geo_deskel_nama."</option>";
		}
	}

	public function getDataBalon(){
		$query = "SELECT * FROM caleg_drh 
				LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
				LEFT JOIN m_geo_kab_kpu ON caleg_drh.tingkat_kabupaten = m_geo_kab_kpu.geo_kab_id
				WHERE";
		$jenis = Input::get('jenis');
		$prov  = Input::get('prov');
		$kota  = Input::get('kota');

		if($jenis != ""){
			$query .= " type='$jenis' ";
			if($prov != ""){
				$query .= " AND ";
			}else if($kota != ""){
				$query .= " AND ";
			}
		}else{
			/*$query .= "WHERE";*/
		}

		if($prov != ""){
			$query .= " tingkat_provinsi = '$prov'";
			if($kota != ""){
				$query .= " AND ";
			}
		}else{
			/*$prov = null;*/
		}

		if($kota != ""){
			$query .= " tingkat_kabupaten = '$kota'";
		}

		$getData = DB::select($query);
		$a = 1;
		foreach ($getData as $get) {
			?>
			<tr>
				<td class="text-center"><?php echo $a++; ?></td>
				<td><?php echo $get->type ?></td>
				<td><?php echo $get->geo_prov_nama ?></td>
				<td><?php echo $get->geo_kab_nama ?></td>
				<td><?php echo $get->nama ?></td>
				<td><?php echo $get->flag ?></td>
				<td class="text-right">
					<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View">
						<i class="fa fa-eye"></i>
					</button>&nbsp;&nbsp;
					<button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
						<i class="fa fa-edit"></i>
					</button>&nbsp;&nbsp;
				</td>
			</tr>
			<?php
		}
	}

	public function getDataBalonProv(){
		$query = "SELECT * FROM caleg_drh 
				LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
				WHERE";
		$jenis = Input::get('jenis');
		$prov  = Input::get('prov');

		if($jenis != ""){
			$query .= " type='$jenis' ";
		}else{
			/*$query .= "WHERE";*/
		}

		if($prov != ""){
			$query .= " AND tingkat_provinsi = '$prov'";
		}else{
			/*$prov = null;*/
		}
		/*echo $query; die();*/
		$getData = DB::select($query);
		$a = 1;
		foreach ($getData as $get) {
			?>
			<tr>
				<td class="text-center"><?php echo $a++; ?></td>
				<td><?php echo $get->type ?></td>
				<td><?php echo $get->geo_prov_nama ?></td>
				<td><?php echo $get->nama ?></td>
				<td><?php echo $get->flag ?></td>
				<td class="text-right">
					<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View" onclick="view_calon_provinsi('<?php echo $get->id ?>')">
								<i class="fa fa-eye"></i>
							</button>&nbsp;&nbsp;
							<button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
								<i class="fa fa-edit"></i>
							</button>&nbsp;&nbsp;
				</td>
			</tr>
			<?php
		}
	}

	public function getDataPilkada(){
		$getJumlahPelaksana = DB::table('caleg_drh')		                     
							 ->where()
		                     ->count();
	}

	public function getDataSurvei(){
		$query = "SELECT caleg_drh.*, m_geo_prov_kpu.geo_prov_nama, m_geo_kab_kpu.geo_kab_nama, t_survei.* FROM caleg_drh 
				JOIN t_survei ON caleg_drh.id = t_survei.id_calon
				LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
				LEFT JOIN m_geo_kab_kpu ON caleg_drh.tingkat_kabupaten = m_geo_kab_kpu.geo_kab_id
				WHERE";
		$jenis = Input::get('jenis');
		$prov  = Input::get('prov');
		$kota  = Input::get('kota');

		if($jenis != ""){
			$query .= " caleg_drh.type='$jenis' ";
			if($prov != ""){
				$query .= " AND ";
			}else if($kota != ""){
				$query .= " AND ";
			}
		}else{
			/*$query .= "WHERE";*/
		}

		if($prov != ""){
			$query .= " caleg_drh.tingkat_provinsi = '$prov'";
			if($kota != ""){
				$query .= " AND ";
			}
		}else{
			/*$prov = null;*/
		}

		if($kota != ""){
			$query .= " caleg_drh.tingkat_kabupaten = '$kota'";
		}

		$query .= " ORDER BY caleg_drh.type DESC";

		$getData = DB::select($query);
		$a = 1;
		foreach ($getData as $get) {
			?>
			<tr>
				<td class="text-center"><?php echo $a++; ?></td>
				<td><?php echo $get->nama ?></td>
				<td class="text-center">
					<?php
					$percentage = ($get->survei / $get->responder)*100;
					echo $percentage." %";
					?>
				</td>
				<td><?php echo $get->geo_prov_nama ?></td>
				<td><?php echo $get->geo_kab_nama ?></td>
				<td><?php echo $get->type ?></td>
				<td></td>
			</tr>
			<?php
		}
	}

	public function getListCalon(){
		$type   = "";
		$d_prov = Input::get('d_prov');
		$d_kota = Input::get('d_kota');

		$getData = DB::table('caleg_drh')
						->where('tingkat_provinsi', $d_prov)
						->where('tingkat_kabupaten', $d_kota)
						->orderBy('type')
						->get();
		$a = 1;
		foreach ($getData as $get) {
			?>
			<tr>
				<td class="text-center"><?php echo $a; ?></td>
				<td>
					<?php echo $get->nama ?>
					<input type="hidden" name="id_calon<?php echo $a; ?>" value="<?php echo $get->id ?>">
				</td>
				<td><?php echo $get->type ?></td>
				<td>
					<input type="number" min="0" step="1" class="form-control inp-number" name="hasil_survei<?php echo $a++; ?>" required>
				</td>
			</tr>
			<?php
		}

		if($a != 1){
		?>
			<tr>
				<td colspan="4">
					<hr>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="text-right">Tidak Menjawab / Tidak tahu</td>
				<td></td>
				<td>
					<input type="number" name="dataPenalti" class="form-control inp-number">
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<button class="btn btn-primary pull-right">SUBMIT</button>
					<input type="hidden" name="jumlahData" value="<?php echo $a-1; ?>">
				</td>
			</tr>
		<?php
		}else{

		}
	}
	function getDataChecklist(){
		$query = "SELECT caleg_drh.*, m_geo_prov_kpu.geo_prov_nama, m_geo_kab_kpu.geo_kab_nama FROM caleg_drh 
				LEFT JOIN m_geo_prov_kpu ON caleg_drh.tingkat_provinsi = m_geo_prov_kpu.geo_prov_id
				LEFT JOIN m_geo_kab_kpu ON caleg_drh.tingkat_kabupaten = m_geo_kab_kpu.geo_kab_id
				WHERE";
		$jenis = Input::get('jenis');
		$prov  = Input::get('prov');
		$kota  = Input::get('kota');

		if($jenis != ""){
			$query .= " type='$jenis' ";
			if($prov != ""){
				$query .= " AND ";
			}else if($kota != ""){
				$query .= " AND ";
			}
		}else{
			/*$query .= "WHERE";*/
		}

		if($prov != ""){
			$query .= " tingkat_provinsi = '$prov'";
			if($kota != ""){
				$query .= " AND ";
			}
		}else{
			/*$prov = null;*/
		}

		if($kota != ""){
			$query .= " tingkat_kabupaten = '$kota'";
		}

		$getData = DB::select($query);
		$a = 1;
		foreach ($getData as $get) {
			?>
			<tr>
				<td class="text-center"><?php echo $a++; ?></td>
				<td><?php echo $get->nama; ?></td>
				<td><?php echo $get->type ?></td>
				<td><?php echo $get->geo_prov_nama ?></td>
				<td><?php echo $get->geo_kab_nama ?></td>
				<td <?php echo ($get->file_riwayat != "" || $get->file_riwayat != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_visi != "" || $get->file_visi != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td  <?php echo ($get->file_ktp != "" || $get->file_ktp != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_kk != "" || $get->file_kk != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td  <?php echo ($get->file_npwp != "" || $get->file_npwp != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_ijazah != "" || $get->file_ijazah != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_skck != "" || $get->file_skck != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_kta != "" || $get->file_kta != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_pendaftaran != "" || $get->file_pendaftaran != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_komitmen != "" || $get->file_komitmen != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_peryataan != "" || $get->file_peryataan != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td <?php echo ($get->file_nkri != "" || $get->file_nkri != null)? "bgcolor='#FF9C00'" : "bgcolor='red'" ?>></td>
				<td class="text-right">
					<a href="<?php echo asset('edit/dokumen').'/'.$get->id ?>" class="btn btn-warning">
						<i class="fa fa-edit"></i>
					</a>
				</td>
			</tr>
			<?php
		}
	}

	public function refreshTable(){
		$getData = DB::table('caleg_drh')
						->join('m_geo_prov_kpu',
								'caleg_drh.tingkat_provinsi',
								'=',
								'm_geo_prov_kpu.geo_prov_id')
						->join('m_geo_kab_kpu',
								'caleg_drh.tingkat_kabupaten',
								'=',
								'm_geo_kab_kpu.geo_kab_id')
						->select('caleg_drh.id',
								'caleg_drh.tempat_lahir',
								'caleg_drh.tanggal_lahir',
								'caleg_drh.type',
								'caleg_drh.nama',
								'm_geo_prov_kpu.geo_prov_nama',
								'm_geo_kab_kpu.geo_kab_nama')
						->where('flag','Balon')
						->get();
		$a = 1;
		foreach ($getData as $get) {
			?>
			<tr>
				<td class="text-center"><?php echo $a++; ?></td>
				<td><?php echo $get->nama ?></td>
				<td><?php echo $get->tempat_lahir.", ".$get->tanggal_lahir ?></td>
				<td><?php echo $get->geo_prov_nama ?></td>
				<td><?php echo $get->geo_kab_nama ?></td>
				<td><?php echo $get->type ?></td>
				<td class="text-right">
					<button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="View" onclick="view_calon_kokab('<?php echo $get->id ?>')">
						<i class="fa fa-eye"></i>
					</button>&nbsp;&nbsp;
					<a href="<?php echo asset('bakal-calon/kokab/edit').'/'.$get->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
						<i class="fa fa-edit"></i>
					</a>&nbsp;&nbsp;
				</td>
			</tr>
			<?php
		}
	}

	public function getDapil($key){
		if($key == 'prov'){
			$prov_id = Input::get('prov_id');
			$getData = DB::table('dapil_dprd_1')
					->where('pro_id', $prov_id)
					->get();

			foreach($getData as $get){
				?>
				<option value="<?php echo $get->dapil_id ?>"><?php echo $get->nama_dapil ?></option>
				<?php
			}
		}else if($key == "kota"){
			$kota = Input::get('kota');
			$getData = DB::table('dapil_dprd_2')
					->where('kab_id', $kota)
					->get();

			foreach($getData as $get){
				?>
				<option value="<?php echo $get->dapil_id ?>"><?php echo $get->nama_dapil ?></option>
				<?php
			}
		}else{

		}
	}
}
?>