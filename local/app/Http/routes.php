<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'PilegController@index');
Route::get('selamat-datang/{id}', 'PilegController@selamat_datang');
Route::get('login', 'DashboardController@index');
Route::get('proses_login', 'DashboardController@proses_login');
Route::get('dashboard', 'DashboardController@view_dashboard');
Route::get('master/level', 'MasterController@view_level');
Route::get('master/user', 'MasterController@view_user');

Route::get('bakal-calon/list', 'BalonController@index_balon');
Route::get('bakal-calon/kokab/add', 'BalonController@open_input_form');
Route::get('bakal-calon/kokab/edit/{key}', 'BalonController@getDataEditKokab');

Route::get('bakal-calon/provinsi', 'BalonController@index_balon_provinsi');
Route::get('bakal-calon/input', 'BalonController@open_input_form_provinsi');
Route::get('bakal-calon/input/add_action', 'BalonController@action_prov');
Route::get('bakal-calon/provinsi/edit/{key}', 'BalonController@getDataEdit');
Route::get('bakal-calon/provinsi/view', 'BalonController@send_data_prov');

Route::get('sk-kokab', 'InputSkController@index_kokab');
Route::get('sk-kokab/edit/{key}', 'InputSkController@edit_kokab');
Route::post('sk-kokab/edit/action', 'InputSkController@kokab_action');

Route::get('sk-provinsi', 'InputSkController@index_provinsi');
Route::get('sk-provinsi/edit/{key}', 'InputSkController@edit_provinsi');

Route::get('checklist', 'ChecklistController@index');
Route::post('checklist/action/edit/{key}', 'ChecklistController@action_edit_dokumen');
Route::get('edit/dokumen/{key}', 'ChecklistController@edit_dokumen');

Route::get('hasil-survei', 'SurveiController@index');
Route::get('hasil-survei/input/survei', 'SurveiController@view_page_input');
Route::get('hasil-survei/edit/survei', 'SurveiController@view_page_edit');
Route::get('hasil-survei/action/survei/{key}', 'SurveiController@action_survei');

Route::get('seleksi/kota', 'SeleksiController@index_kokab');

Route::get('agenda', 'AgendaController@index');
Route::get('agenda/tambah', 'AgendaController@view_add');
Route::get('agenda/add_action', 'AgendaController@add_action');
Route::get('agenda/add_action_n', 'AgendaController@add_action_n');
/*Route::get('home', 'HomeController@index');*/

/*ROUTING AJAX*/
Route::get('load-data-level', 'MasterController@load_level');
Route::get('ajaxAddLevel', 'MasterController@add_level');
Route::get('ajaxEditLevel', 'MasterController@edit_level');
Route::get('ajaxDeleteLevel', 'MasterController@delete_level');
Route::get('ajaxGetLevel', 'MasterController@get_level');
Route::get('ajaxCekID', 'AjaxController@cek_identitas');


Route::get('ajaxGetKabupaten', 'AjaxController@getKab');
Route::get('ajaxKecamatan', 'AjaxController@getKecamatan');
Route::get('ajaxKelurahan', 'AjaxController@getKelurahan');

Route::get('ajaxDataBalon', 'AjaxController@getDataBalon');
Route::get('ajaxDataBalonProv', 'AjaxController@getDataBalonProv');
Route::get('ajaxGetDataPilkada', 'AjaxController@getDataPilkada');
Route::get('ajaxDataSurvei', 'AjaxController@getDataSurvei');
Route::get('ajaxGetListCalon', 'AjaxController@getListCalon');
Route::get('ajaxDataChecklist', 'AjaxController@getDataChecklist');
Route::get('ajaxRefreshDataBalon', 'AjaxController@refreshTable');

Route::get('ajax/dapil/{key}', 'AjaxController@getDapil');

Route::get('throw-data', 'AjaxController@data_balon');

Route::get('load-data-user', 'MasterController@load_user');
Route::get('ajaxAddUser', 'MasterController@add_user');

Route::get('logout', 'DashboardController@logout');

/* ROUTING GROUP FOR USER */
Route::get('user/dashboard', 'ControllerUserMandiri@index');
Route::get('user/edit/{key}/biodata','ControllerUserMandiri@edit_bio');
/* END OF ROUTING GROUP */
Route::post('insertToDB', 'PilegController@input_data_calon');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
