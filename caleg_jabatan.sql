/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : 127.0.0.1:3306
Source Database       : sicaleg

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-08-21 11:30:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for caleg_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `caleg_jabatan`;
CREATE TABLE `caleg_jabatan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `caleg_id` int(6) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `type` enum('1','2') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of caleg_jabatan
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
